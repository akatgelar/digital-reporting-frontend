import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { UserRoutes } from './user.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(UserRoutes),
      SharedModule
  ],
  declarations: []
})

export class UserModule {}
