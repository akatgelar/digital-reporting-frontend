import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { UserListComponent } from './user-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const UserListRoutes: Routes = [
  {
    path: '',
    component: UserListComponent,
    data: {
      breadcrumb: 'List User',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [UserListComponent]
})
export class UserListModule { }
