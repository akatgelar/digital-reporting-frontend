import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { IOption } from "ng-select";

const now = new Date();

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class UserAddComponent implements OnInit {

  public loading = false;
  private url_user = '/api/users';
  private url_dinas = '/api/dinas';
  private url_bidang = '/api/bidang';
  private url_seksi = '/api/seksi';
  private url_jabatan = '/api/jabatan';
  private url_pangkat = '/api/pangkat';
  private url_role = '/api/users-permissions/roles';

  myForm: FormGroup;
  submitted: boolean;

  lahirMinPicker: NgbDateStruct = {
    year: now.getFullYear() - 75,
    month: now.getMonth() + 1,
    day: now.getDate()
  };

  lahirMaxPicker: NgbDateStruct = {
    year: now.getFullYear(),
    month: now.getMonth() + 1,
    day: now.getDate()
  };

  dinasOption: Array<IOption> = [];
  bidangOption: Array<IOption> = [];
  seksiOption: Array<IOption> = [];
  jabatanOption: Array<IOption> = [];
  pangkatOption: Array<IOption> = [];
  roleOption: Array<IOption> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('user', 'add')){
      this.router.navigate(['/error/403']);
    }

    this.getDataDinasOption();
    this.getDataJabatanOption();
    this.getDataPangkatOption();
    this.getDataRoleOption();

    this.myForm = new FormGroup({
      nip: new FormControl(),
      kode_karyawan: new FormControl(),
      nama: new FormControl({value: '', disabled: false}, [Validators.required]),
      jenis_kelamin: new FormControl({value: '', disabled: false}, [Validators.required]),
      tempat_lahir: new FormControl({value: '', disabled: false}, [Validators.required]),
      tanggal_lahir: new FormControl({value: this.lahirMaxPicker, disabled: false}, [Validators.required]),
      dinas: new FormControl({value: '', disabled: false}, [Validators.required]),
      bidang: new FormControl({value: '', disabled: false}, [Validators.required]),
      seksi: new FormControl({value: '', disabled: false}),
      // seksi: new FormControl({value: '', disabled: false}, [Validators.required]),
      jabatan: new FormControl({value: '', disabled: false}, [Validators.required]),
      pangkat: new FormControl({value: '', disabled: false}),
      // pangkat: new FormControl({value: '', disabled: false}, [Validators.required]),
      alamat: new FormControl({value: '', disabled: false}, [Validators.required]),
      no_telp: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[0-9]*$")]),
      keterangan: new FormControl(),
      role: new FormControl({value: '', disabled: false}, [Validators.required]),
      email: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      username: new FormControl({value: '', disabled: false}, [Validators.required, Validators.minLength(5)]),
      password: new FormControl({value: '', disabled: false}, [Validators.required, Validators.minLength(6)]),
      // password_konfirmasi: new FormControl({value: '', disabled: false}, [Validators.required, Validators.minLength(6)]),
      is_deleted: new FormControl(),
      created_at: new FormControl(),
      updated_at: new FormControl(),
    },
      // this.passwordMatchValidator
    );
  }

  get nama() { return this.myForm.get('nama'); }
  get jenis_kelamin() { return this.myForm.get('jenis_kelamin'); }
  get tempat_lahir() { return this.myForm.get('tempat_lahir'); }
  get tanggal_lahir() { return this.myForm.get('tanggal_lahir'); }
  get dinas() { return this.myForm.get('dinas'); }
  get bidang() { return this.myForm.get('bidang'); }
  get seksi() { return this.myForm.get('seksi'); }
  get jabatan() { return this.myForm.get('jabatan'); }
  get pangkat() { return this.myForm.get('pangkat'); }
  get alamat() { return this.myForm.get('alamat'); }
  get no_telp() { return this.myForm.get('no_telp'); }
  get role() { return this.myForm.get('role'); }
  get email() { return this.myForm.get('email'); }
  get username() { return this.myForm.get('username'); }
  get password() { return this.myForm.get('password'); }

  // passwordMatchValidator(group: FormGroup) {
  //   let pass = group.get('password').value;
  //   let confirmPass = group.get('password_konfirmasi').value;

  //   return pass === confirmPass ? null : { 'mismatch': true };
  // }

  // DINAS OPTION ======================================================================================================
  getDataDinasOption() {
    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '?_sort=nama_pendek:ASC', json_dinas).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var dinasOpt = result_msg.map( function(dinas) {
            var option = {
              value: dinas.id,
              label: dinas.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.dinasOption = dinasOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dinasOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dinasOption = null;
        console.log(error);
      }
    );
  }

  onSelectedDinas(option: IOption) {
    const id_dinas = option.value;
    this.getDataBidangOption(id_dinas);
  }

  onDeselectedDinas(option: IOption) {
    this.bidangOption = null;
    this.seksiOption = null;
    this.myForm.controls['bidang'].reset();
    this.myForm.controls['seksi'].reset();
  }

  // BIDANG OPTION =====================================================================================================
  getDataBidangOption(id_dinas) {
    const json_bidang = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_bidang + '?dinas=' + id_dinas + '&_sort=nama_pendek:ASC', json_bidang).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var bidangOpt = result_msg.map( function(bidang) {
            var option = {
              value: bidang.id,
              label: bidang.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.bidangOption = bidangOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.bidangOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.bidangOption = null;
        console.log(error);
      }
    );
  }

  onSelectedBidang(option: IOption) {
    const id_bidang = option.value;
    this.getDataSeksiOption(id_bidang);
  }

  onDeselectedBidang(option: IOption) {
    this.seksiOption = null;
    this.myForm.controls['seksi'].reset();
  }

  // SEKSI OPTION ======================================================================================================
  getDataSeksiOption(id_bidang) {
    const json_seksi = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_seksi + '?bidang=' + id_bidang + '&_sort=nama_pendek:ASC', json_seksi).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var seksiOpt = result_msg.map( function(seksi) {
            var option = {
              value: seksi.id,
              label: seksi.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.seksiOption = seksiOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.seksiOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.bidangOption = null;
        console.log(error);
      }
    );
  }

  // JABATAN OPTION ====================================================================================================
  getDataJabatanOption() {
    const json_jabatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_jabatan + '?_sort=nama:ASC', json_jabatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var jabatanOpt = result_msg.map( function(jabatan) {
            var option = {
              value: jabatan.id,
              label: jabatan.nama
            }
            return option;
          });
          this.loading = false;
          this.jabatanOption = jabatanOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.jabatanOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.jabatanOption = null;
        console.log(error);
      }
    );
  }

  // PANGKAT OPTION ====================================================================================================
  getDataPangkatOption() {
    const json_pangkat = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_pangkat + '?_sort=nama:ASC', json_pangkat).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var pangkatOpt = result_msg.map( function(pangkat) {
            var option = {
              value: pangkat.id,
              label: pangkat.nama
            }
            return option;
          });
          this.loading = false;
          this.pangkatOption = pangkatOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.pangkatOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.pangkatOption = null;
        console.log(error);
      }
    );
  }

  // ROLE OPTION ====================================================================================================
  getDataRoleOption() {
    const json_role = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_role + '?_sort=nama:ASC', json_role).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var roleOpt = result_msg.roles.map( function(role) {
            var option = {
              value: role.id,
              label: role.name
            }
            return option;
          });
          this.loading = false;
          this.roleOption = roleOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.roleOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.roleOption = null;
        console.log(error);
      }
    );
  }

  // DATEPICKER ========================================================================================================
  closeFix(event, datePicker) {
    if(event.target.offsetParent == null)
      datePicker.close();
    else if(event.target.offsetParent.nodeName != "NGB-DATEPICKER")
      datePicker.close();
  }

  // SAVE ==============================================================================================================
  onSubmit() {
    this.submitted = true;
    let val_user = this.myForm.value;

    let tanggal = this.myForm.value.tanggal_lahir;

    this.myForm.patchValue({
      provider: 'local',
      confirmed: true,
      is_deleted: false,
      tanggal_lahir: this.convert.formatDateYMD(new Date(tanggal.year, tanggal.month - 1, tanggal.day).toString()),
      status_pegawai: true,
      created_at: this.convert.formatDateYMDHMS(new Date().toString()),
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    });

    val_user = this.myForm.value;
    let json_user = JSON.stringify(val_user);

    this.loading = true;
    this.httpRequest.httpPost(this.url_user, json_user).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);

          this.loading = false;
          this.showSuccess(result_msg['nama']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'User ' + nama + ' berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/user/list'])
    });
  }

}
