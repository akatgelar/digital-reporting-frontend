import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { UserAddComponent } from './user-add.component';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';

export const UserAddRoutes: Routes = [
  {
    path: '',
    component: UserAddComponent,
    data: {
      breadcrumb: 'Tambah User',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserAddRoutes),
    SharedModule,
    ShowHidePasswordModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [UserAddComponent]
})
export class UserAddModule { }
