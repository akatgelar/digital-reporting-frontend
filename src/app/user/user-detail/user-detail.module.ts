import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { UserDetailComponent } from './user-detail.component';
import { NgxLoadingModule } from 'ngx-loading';

export const UserDetailRoutes: Routes = [
  {
    path: '',
    component: UserDetailComponent,
    data: {
      breadcrumb: 'Detail User',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserDetailRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [UserDetailComponent]
})
export class UserDetailModule { }
