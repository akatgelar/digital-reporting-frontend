import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class UserDetailComponent implements OnInit {

  public loading = false;
  private url_user = '/api/users';

  dataUser = [];
  dataRole = [];
  dataDinas = [];
  dataBidang = [];
  dataSeksi = [];
  dataJabatan = [];
  dataPangkat = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('user', 'detail')){
      this.router.navigate(['/error/403']);
    }

    let id = this.route.snapshot.paramMap.get('id');
    this.getDataUser(id);
  }

  getDataUser(id) {
    const json_user = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_user + '/'+ id, json_user).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataUser = result_msg;
          this.dataDinas = result_msg.dinas;
          this.dataBidang = result_msg.bidang;
          this.dataSeksi = result_msg.seksi;
          this.dataJabatan = result_msg.jabatan;
          this.dataPangkat = result_msg.pangkat;
          this.dataRole = result_msg.role;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataUser = null;
          this.dataDinas = null;
          this.dataBidang = null;
          this.dataSeksi = null;
          this.dataJabatan = null;
          this.dataPangkat = null;
          this.dataRole = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
          this.dataUser = null;
          this.dataDinas = null;
          this.dataBidang = null;
          this.dataSeksi = null;
          this.dataJabatan = null;
          this.dataPangkat = null;
          this.dataRole = null;
        console.log(error);
      }
    );
  }

}
