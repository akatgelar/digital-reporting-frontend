import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { UserEditComponent } from './user-edit.component';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';

export const UserEditRoutes: Routes = [
  {
    path: '',
    component: UserEditComponent,
    data: {
      breadcrumb: 'Ubah User',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserEditRoutes),
    SharedModule,
    ShowHidePasswordModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [UserEditComponent]
})
export class UserEditModule { }
