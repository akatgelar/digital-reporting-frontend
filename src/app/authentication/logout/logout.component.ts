import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { SessionService } from '../../shared/service/session.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit {
  public loading = false;
  private login_url = '/api/auth/local';

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private session: SessionService,
    private router: Router) {

  }

  ngOnInit() {

    this.session.logOut();
    this.router.navigate(['/auth/login']);
  }

}
