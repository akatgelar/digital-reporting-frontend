import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [
    HttpRequestService,
    SessionService,
    ErrorMessageService,
    ConverterService
  ],
})

export class LoginComponent implements OnInit {

  public loading = false;
  private url_login = '/api/auth/local';

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService,
    // public menuItems: MenuItems
  ) {}

  ngOnInit() {
    this.myForm = new FormGroup({
      identifier: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  onSubmit() {
    this.submitted = true;
    const val_login = this.myForm.value;
    const json_login = JSON.stringify(val_login);

    this.loading = true;
    this.httpRequest.httpLogin(this.url_login, json_login).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          let role = result_msg['user']['role']['type'];
          role = role.toString().toLowerCase();
          role = role.replace('_', '-');

          this.session.logIn(result_msg['jwt'], JSON.stringify(result_msg['user']), JSON.stringify({}));
          this.loading = false;
          this.router.navigate(['/dashboard']);
          
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }
}
