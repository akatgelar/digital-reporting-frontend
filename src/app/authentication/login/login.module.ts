import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../shared/shared.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from "./login.component";
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

import {WithSocialComponent} from "./with-social/with-social.component";
import {WithHeaderFooterComponent} from "./with-header-footer/with-header-footer.component";
import {WithBgImageComponent} from "./with-bg-image/with-bg-image.component";
import {WithSocialHeaderFooterComponent} from './with-social-header-footer/with-social-header-footer.component';

export const LoginRoutes: Routes = [
    {
        path: '',
        component: LoginComponent,
        data: {
            breadcrumb: 'Login'
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LoginRoutes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [
        LoginComponent,
        WithBgImageComponent, 
        WithHeaderFooterComponent, 
        WithSocialComponent, 
        WithSocialHeaderFooterComponent
    ],
    bootstrap: [LoginComponent]
})

export class LoginModule {}
