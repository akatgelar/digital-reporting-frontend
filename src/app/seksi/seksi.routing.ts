import { Routes } from '@angular/router';

export const SeksiRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Seksi',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './seksi-list/seksi-list.module#SeksiListModule'
      },
      {
        path: 'add',
        loadChildren: './seksi-add/seksi-add.module#SeksiAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './seksi-detail/seksi-detail.module#SeksiDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './seksi-edit/seksi-edit.module#SeksiEditModule'
      }
    ]
  }
]
