import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { SeksiDetailComponent } from './seksi-detail.component';
import { NgxLoadingModule } from 'ngx-loading';

export const SeksiDetailRoutes: Routes = [
  {
    path: '',
    component: SeksiDetailComponent,
    data: {
      breadcrumb: 'Detail Seksi',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SeksiDetailRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [SeksiDetailComponent]
})
export class SeksiDetailModule { }
