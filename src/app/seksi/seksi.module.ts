import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { SeksiRoutes } from './seksi.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(SeksiRoutes),
      SharedModule
  ],
  declarations: []
})

export class SeksiModule {}
