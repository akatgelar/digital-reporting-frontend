import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { SeksiListComponent } from './seksi-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const SeksiListRoutes: Routes = [
  {
    path: '',
    component: SeksiListComponent,
    data: {
      breadcrumb: 'List Seksi',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SeksiListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [SeksiListComponent]
})
export class SeksiListModule { }
