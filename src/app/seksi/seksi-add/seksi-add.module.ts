import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { SeksiAddComponent } from './seksi-add.component';
import { NgxLoadingModule } from 'ngx-loading';

export const SeksiAddRoutes: Routes = [
  {
    path: '',
    component: SeksiAddComponent,
    data: {
      breadcrumb: 'Tambah Seksi',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SeksiAddRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [SeksiAddComponent]
})
export class SeksiAddModule { }
