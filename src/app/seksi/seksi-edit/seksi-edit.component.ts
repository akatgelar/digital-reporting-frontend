import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';
import { IOption } from "ng-select";

@Component({
  selector: 'app-seksi-edit',
  templateUrl: './seksi-edit.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class SeksiEditComponent implements OnInit {

  public loading = false;
  private url_seksi = '/api/seksi';
  private url_bidang = '/api/bidang';
  private url_dinas = '/api/dinas';

  dataSeksi = [];
  dinasOption: Array<IOption> = [];
  bidangOption: Array<IOption> = [];
  // bidangDinas: number;

  myForm: FormGroup;
  submitted: boolean;
  dinas: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('seksi','edit')){
      this.router.navigate(['/error/403']);
    }

    let id = this.route.snapshot.paramMap.get('id');
    this.getDataSeksi(id);

    this.getDataDinasOption();
    // this.getDataBidangOption(this.bidangDinas);

    this.myForm = new FormGroup({
      dinas: new FormControl({value: '', disabled: false}, [Validators.required]),
      bidang: new FormControl({value: '', disabled: false}, [Validators.required]),
      nama_pendek: new FormControl({value: '', disabled: false}, [Validators.required]),
      nama_panjang: new FormControl({value: '', disabled: false}, [Validators.required]),
      email: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      is_deleted: new FormControl(),
      created_at: new FormControl(),
      updated_at: new FormControl(),
    });
  }

  getDataSeksi(id) {
    const json_seksi = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_seksi + '/'+ id, json_seksi).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataSeksi = result_msg;
          this.getDataBidangOption(result_msg.dinas.id);
          // this.bidangDinas = result_msg.dinas.id;
          // this.dinas = result_msg.dinas.id;
          // this.dinasSelectedOption = '1';
          this.setForm(this.dataSeksi);
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataSeksi = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataSeksi = null;
        console.log(error);
      }
    );
  }

  setForm(data){
    this.myForm.patchValue({
      id: data.id,
      dinas: data.dinas.id.toString(),
      bidang: data.bidang.id.toString(),
      nama_pendek: data.nama_pendek,
      nama_panjang: data.nama_panjang,
      email: data.email
    });
  }

  getDataDinasOption() {
    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '?_sort=nama_pendek:ASC', json_dinas).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var dinasOpt = result_msg.map( function(dinas) {
            var option = {
              value: dinas.id.toString(),
              label: dinas.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.dinasOption = dinasOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dinasOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dinasOption = null;
        console.log(error);
      }
    );
  }

  getDataBidangOption(id_dinas) {
    const json_bidang = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_bidang + '?dinas=' + id_dinas + '&_sort=nama_pendek:ASC', json_bidang).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var bidangOpt = result_msg.map( function(bidang) {
            var option = {
              value: bidang.id.toString(),
              label: bidang.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.bidangOption = bidangOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.bidangOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.bidangOption = null;
        console.log(error);
      }
    );
  }

  onSelectedDinas(option: IOption) {
    const id_dinas = option.value;
    this.getDataBidangOption(id_dinas);
    this.myForm.patchValue({
      bidang: ''
    });
  }

  onDeselectedDinas(option: IOption) {
    this.bidangOption = null;
    this.myForm.controls['bidang'].reset();
  }

  onSubmit() {
    let id = this.route.snapshot.paramMap.get('id');

    this.submitted = true;
    let val_seksi = this.myForm.value;

    this.myForm.patchValue({
      is_deleted: false,
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    });

    val_seksi = this.myForm.value;
    let json_seksi = JSON.stringify(val_seksi);

    this.loading = true;
    this.httpRequest.httpPut(this.url_seksi + '/' + id, json_seksi).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.showSuccess(result_msg['nama_pendek']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'Seksi ' + nama + ' berhasil diubah.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/seksi/list'])
    });
  }

}
