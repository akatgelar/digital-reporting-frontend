import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { SeksiEditComponent } from './seksi-edit.component';
import { NgxLoadingModule } from 'ngx-loading';

export const SeksiEditRoutes: Routes = [
  {
    path: '',
    component: SeksiEditComponent,
    data: {
      breadcrumb: 'Ubah Seksi',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SeksiEditRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [SeksiEditComponent]
})
export class SeksiEditModule { }
