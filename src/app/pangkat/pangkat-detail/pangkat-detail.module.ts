import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { PangkatDetailComponent } from './pangkat-detail.component';
import { NgxLoadingModule } from 'ngx-loading';

export const PangkatDetailRoutes: Routes = [
  {
    path: '',
    component: PangkatDetailComponent,
    data: {
      breadcrumb: 'Detail Pangkat',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PangkatDetailRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [PangkatDetailComponent]
})
export class PangkatDetailModule { }
