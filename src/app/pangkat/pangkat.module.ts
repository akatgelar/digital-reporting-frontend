import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { PangkatRoutes } from './pangkat.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PangkatRoutes),
      SharedModule
  ],
  declarations: []
})

export class PangkatModule {}
