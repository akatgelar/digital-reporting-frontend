import { Routes } from '@angular/router';

export const PangkatRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Pangkat',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './pangkat-list/pangkat-list.module#PangkatListModule'
      },
      {
        path: 'add',
        loadChildren: './pangkat-add/pangkat-add.module#PangkatAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './pangkat-detail/pangkat-detail.module#PangkatDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './pangkat-edit/pangkat-edit.module#PangkatEditModule'
      }
    ]
  }
]
