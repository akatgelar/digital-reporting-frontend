import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { PangkatEditComponent } from './pangkat-edit.component';
import { NgxLoadingModule } from 'ngx-loading';

export const PangkatEditRoutes: Routes = [
  {
    path: '',
    component: PangkatEditComponent,
    data: {
      breadcrumb: 'Ubah Pangkat',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PangkatEditRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [PangkatEditComponent]
})
export class PangkatEditModule { }
