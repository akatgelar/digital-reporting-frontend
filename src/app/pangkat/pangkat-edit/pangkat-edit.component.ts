import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-pangkat-edit',
  templateUrl: './pangkat-edit.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class PangkatEditComponent implements OnInit {

  public loading = false;
  private url_pangkat = '/api/pangkat';

  dataPangkat = [];

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('pangkat','edit')){
      this.router.navigate(['/error/403']);
    }

    let id = this.route.snapshot.paramMap.get('id');
    this.getDataPangkat(id);

    this.myForm = new FormGroup({
      nama: new FormControl({value: '', disabled: false}, [Validators.required]),
      keterangan: new FormControl({value: '', disabled: false}),
      is_deleted: new FormControl(),
      updated_at: new FormControl(),
    });
  }

  getDataPangkat(id) {
    const json_pangkat = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_pangkat + '/'+ id, json_pangkat).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPangkat = result_msg;
          this.setForm(this.dataPangkat);
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataPangkat = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataPangkat = null;
        console.log(error);
      }
    );
  }

  setForm(data){
    this.myForm.patchValue({
      id: data.id,
      nama: data.nama,
      keterangan: data.keterangan
    });
  }

  onSubmit() {
    let id = this.route.snapshot.paramMap.get('id');

    this.submitted = true;
    let val_pangkat = this.myForm.value;

    this.myForm.patchValue({
      is_deleted: false,
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    });

    val_pangkat = this.myForm.value;
    let json_pangkat = JSON.stringify(val_pangkat);

    this.loading = true;
    this.httpRequest.httpPut(this.url_pangkat + '/' + id, json_pangkat).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.showSuccess(result_msg['nama']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'Pangkat ' + nama + ' berhasil diubah.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/pangkat/list'])
    });
  }

}
