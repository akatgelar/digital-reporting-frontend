import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { PangkatAddComponent } from './pangkat-add.component';
import { NgxLoadingModule } from 'ngx-loading';

export const PangkatAddRoutes: Routes = [
  {
    path: '',
    component: PangkatAddComponent,
    data: {
      breadcrumb: 'Tambah Pangkat',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PangkatAddRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [PangkatAddComponent]
})
export class PangkatAddModule { }
