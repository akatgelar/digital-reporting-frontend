import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { PangkatListComponent } from './pangkat-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const PangkatListRoutes: Routes = [
  {
    path: '',
    component: PangkatListComponent,
    data: {
      breadcrumb: 'List Pangkat',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PangkatListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [PangkatListComponent]
})
export class PangkatListModule { }
