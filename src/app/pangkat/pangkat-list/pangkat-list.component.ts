import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import swal from 'sweetalert2';
import { IOption } from "ng-select";

@Component({
  selector: 'app-pangkat-list',
  templateUrl: './pangkat-list.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class PangkatListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_pangkat = '/api/pangkat';

  rowsFilter = [];
  tempFilter = [];
  limit = 10;
  messages = {
    emptyMessage: 'Tidak ada data.',
    totalMessage: 'total'
  }

  pencarianType: string;
  pencarianInput: boolean = true;
  pencarianOption: Array<IOption> = [
    {value: 'nama', label: 'Nama'},
    {value: 'keterangan', label: 'Keterangan'}
  ];

  public currentPageLimit: number = 10;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];

  constructor(
    private router: Router,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('pangkat','list')){
      this.router.navigate(['/error/403']);
    }

    this.getData();
  }

  onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }

  changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const filter = '&' + this.pencarianType + '_contains=' + val;

    const json_jabatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_pangkat + '?is_deleted=false&_sort=updated_at:desc' + filter, json_jabatan).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          this.rowsFilter = result_msg;
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  getData() {
    const json_pangkat = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_pangkat + '?is_deleted=false&_sort=updated_at:desc', json_pangkat).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.rowsFilter = result_msg;
          this.loading = false;
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onDelete(row);
      }
    });
  }

  onDelete(row) {
    let json_pangkat = {
      is_deleted: true,
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    };

    this.loading = true;
    this.httpRequest.httpPut(this.url_pangkat + '/' + row['id'], json_pangkat).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.showSuccess(row['nama']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      html: 'Pangkat ' + nama + ' berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.getData();
    });
  }

  onSelectedPencarian(value) {
    this.pencarianType = value;
    this.pencarianInput = value == 0 ? true : false;
  }

}
