import { Routes } from '@angular/router';

export const KegiatanRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Kegiatan',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './kegiatan-list/kegiatan-list.module#KegiatanListModule'
      },
      {
        path: 'add',
        loadChildren: './kegiatan-add/kegiatan-add.module#KegiatanAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './kegiatan-detail/kegiatan-detail.module#KegiatanDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './kegiatan-edit/kegiatan-edit.module#KegiatanEditModule'
      }
    ]
  }
]
