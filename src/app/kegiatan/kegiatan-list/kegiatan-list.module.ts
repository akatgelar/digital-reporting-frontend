import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { KegiatanListComponent } from './kegiatan-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const KegiatanListRoutes: Routes = [
  {
    path: '',
    component: KegiatanListComponent,
    data: {
      breadcrumb: 'List Kegiatan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(KegiatanListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [KegiatanListComponent]
})
export class KegiatanListModule { }
