import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import swal from 'sweetalert2';
import { IOption } from "ng-select";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

const now = new Date();

@Component({
  selector: 'app-kegiatan-list',
  templateUrl: './kegiatan-list.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class KegiatanListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_kegiatan = '/api/kegiatan';

  is_list = false;
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 

  dataKegiatan = [];

  rowsFilter = [];
  tempFilter = [];
  limit = 10;
  messages = {
    emptyMessage: 'Tidak ada data.',
    totalMessage: 'total'
  }

  pencarianType: string;
  pencarianInput: boolean = true;
  pencarianOption: Array<IOption> = [
    {value: 'tanggal', label: 'Tanggal'},
    {value: 'nama_kegiatan', label: 'Kegiatan'}
  ];

  tanggalTodayPicker: NgbDateStruct = {
    year: now.getFullYear(), 
    month: now.getMonth() + 1, 
    day: now.getDate()
  };

  public currentPageLimit: number = 10;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];

  constructor(
    private router: Router,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('kegiatan','list')){
      this.router.navigate(['/error/403']);
    }
 
    this.is_list = this.session.checkAccess('kegiatan','list');
    this.is_detail = this.session.checkAccess('kegiatan','detail');
    this.is_add = this.session.checkAccess('kegiatan','add');
    this.is_edit = this.session.checkAccess('kegiatan','edit');
    this.is_delete = this.session.checkAccess('kegiatan','delete'); 

    this.getData();
  }

  onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        // TODO[Dmitry Teplov] test with server-side paging.
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }

  changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }

  updateFilter(event: any) {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    console.log(sess['role']['type'])

    if(this.pencarianType == 'tanggal'){
      if(!event){
         var pan = '';
      } else {
        var val = event;
        var thn = val.year.toString();
        var bln = (val.month < 10) ? '0' + val.month.toString() : val.month.toString();
        var day = (val.day < 10) ? '0' + val.day.toString() : val.day.toString();
        var tanggal = thn + '-' + bln + '-' + day;
        var pan = tanggal;
        var fil = 'tanggal_gte=' + tanggal + '&tanggal_lte=' + tanggal;
      }
    } else if(this.pencarianType == 'nama_kegiatan'){
      var val = event.target.value.toLowerCase();
      var pan: string = val;
      var fil = 'nama_kegiatan_contains=' + val;
    }

    // const id = '3';
    const id         = sess['id'];
    const user       = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
    const is_deleted = 'is_deleted=false';
    const sort       = '_sort=tanggal:desc,id:desc';

    if(pan.length > 0){
      var filter = '?' + user + is_deleted + '&' + sort + '&' + fil;
    } else {
      var filter = '?' + user + is_deleted + '&' + sort;
    }

    const json_jabatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatan + filter, json_jabatan).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          this.rowsFilter = result_msg;
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  getData() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    // const id = '3';
    const id         = sess['id'];
    const user       = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
    const is_deleted = 'is_deleted=false';
    const sort       = '_sort=tanggal:desc,id:desc';
    const filter     = '?' + user + is_deleted + '&' + sort;

    const json_kegiatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatan + filter, json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          this.loading = false; 
          const result_msg = JSON.parse(result._body);
          this.rowsFilter = result_msg;

        } catch (error) {
          this.loading = false; 
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false; 
        console.log(error);
      }
    );
  }

  showDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onDelete(row);
      }
    });
  }

  onDelete(row) {
    let json_kegiatan = {
      is_deleted: true,
      updated_at: new Date()
    };

    this.loading = true;
    this.httpRequest.httpPut(this.url_kegiatan + '/' + row['id'], json_kegiatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.showSuccess();
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(){
    swal({
      title: 'Informasi',
      html: 'Kegiatan berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.getData();
    });
  }

  onSelectedPencarian(value) {
    this.pencarianType = value;
    this.pencarianInput = value == 0 ? true : false;
  }

  // DATEPICKER ========================================================================================================
  closeFix(event, datePicker) {
    if(event.target.offsetParent == null)
      datePicker.close();
    else if(event.target.offsetParent.nodeName != "NGB-DATEPICKER")
      datePicker.close();
  }

}
