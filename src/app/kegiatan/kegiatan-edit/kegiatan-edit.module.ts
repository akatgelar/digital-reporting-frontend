import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// import { CKEditorModule } from 'ng2-ckeditor';

import { SharedModule } from '../../shared/shared.module';
import { KegiatanEditComponent } from './kegiatan-edit.component';
import { FileDropModule } from 'ngx-file-drop';
import { NgxLoadingModule } from 'ngx-loading';

export const KegiatanEditRoutes: Routes = [
  {
    path: '',
    component: KegiatanEditComponent,
    data: {
      breadcrumb: 'Tambah Kegiatan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(KegiatanEditRoutes),
    SharedModule,
    FileDropModule,
    NgxLoadingModule.forRoot({}),
    // CKEditorModule
  ],
  declarations: [KegiatanEditComponent]
})
export class KegiatanEditModule { }
