import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';
import { fadeInOutTranslate } from '../../shared/elements/animation';

import swal from 'sweetalert2';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

const now = new Date();

@Component({
  selector: 'app-kegiatan-edit',
  templateUrl: './kegiatan-edit.component.html',
  styleUrls: ['../kegiatan.component.css'],
  animations: [fadeInOutTranslate],
  encapsulation: ViewEncapsulation.None,
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class KegiatanEditComponent implements OnInit {

  public loading = false;
  private url_kegiatan = '/api/kegiatan';
  private url_user = '/api/users';
  private url_upload = '/api/upload'; 
  private url_evidence = '/api/evidence'; 
  private url_kegiatanevidence = '/api/kegiatanevidence'; 

  dataUser: any[] = [];
  dataKegiatan = [];
  files: UploadFile[] = [];

  myForm: FormGroup;

  upload_gambar: FormArray; 
  upload_gambar_old: any[] = []; 
  fileUploads: any[] = []; 
  fileSayas: any[] = []; 

  submitted: boolean;

  currentSayaStart: number = 0;

  tanggalMinPicker: NgbDateStruct = {
    year: now.getFullYear(), 
    month: now.getMonth() - 1, 
    day: now.getDate()
  };

  tanggalTodayPicker: NgbDateStruct = {
    year: now.getFullYear(), 
    month: now.getMonth() + 1, 
    day: now.getDate()
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('kegiatan', 'edit')){
      this.router.navigate(['/error/403']);
    }

    let id = this.route.snapshot.paramMap.get('id');
    this.getDataKegiatan(id);
    this.getDataKegiatanEvidence(id);

    this.getDataSaya();

    this.myForm = new FormGroup({
      // tahun: new FormControl(),
      // bulan: new FormControl(),
      // hari: new FormControl(),
      tanggal: new FormControl({value: this.tanggalTodayPicker, disabled: false}, [Validators.required]),
      // tanggal_kegiatan: new FormControl(),
      nama_kegiatan: new FormControl({value: '', disabled: false}, [Validators.required]),
      tempat: new FormControl({value: '', disabled: false}, [Validators.required]),
      penyelenggara: new FormControl({value: '', disabled: false}, [Validators.required]),
      trigger: new FormControl(),
      is_tugas_pokok: new FormControl({value: '', disabled: false}, [Validators.required]),
      keterangan: new FormControl({value: '', disabled: false}, [Validators.required]),
      upload_gambar: new FormArray([]),
      is_deleted: new FormControl(),
      updated_at: new FormControl(),
    },
      // this.passwordMatchValidator
    );  
  }

  // VALIDATION ========================================================================================================
  get tanggal() { return this.myForm.get('tanggal'); }
  get nama_kegiatan() { return this.myForm.get('nama_kegiatan'); }
  get tempat() { return this.myForm.get('tempat'); }
  get penyelenggara() { return this.myForm.get('penyelenggara'); }
  get is_tugas_pokok() { return this.myForm.get('is_tugas_pokok'); }
  get keterangan() { return this.myForm.get('keterangan'); }

  // KEGIATAN ==========================================================================================================
  getDataKegiatan(id) {
    const json_kegiatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatan + '/'+ id, json_kegiatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataKegiatan = result_msg;
          this.setForm(this.dataKegiatan);
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataKegiatan = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataKegiatan = null;
        console.log(error);
      }
    );
  }

  setForm(data){
    let tanggal = new Date(data.tanggal);
    let tanggalPicker = {
      year: tanggal.getFullYear(), 
      month: tanggal.getMonth() + 1, 
      day: tanggal.getDate()
    };

    this.myForm.patchValue({
      tanggal: tanggalPicker,
      nama_kegiatan: data.nama_kegiatan,
      tempat: data.tempat,
      penyelenggara: data.penyelenggara,
      trigger: data.trigger,
      is_tugas_pokok: data.is_tugas_pokok,
      keterangan: data.keterangan
    });
  }

  // EVIDENCE ==========================================================================================================
  getDataKegiatanEvidence(id) {
    const json_ke = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatanevidence + '?is_deleted=false&kegiatan='+ id, json_ke).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);

          for(let res of result_msg){
            this.upload_gambar = this.myForm.get('upload_gambar') as FormArray;
            this.upload_gambar.push( 
              new FormGroup({
                name: new FormControl({value: res.evidence.nama, disabled: false}, []), 
                value: new FormControl({value: res.evidence.id, disabled: false}, []), 
              })
            );
            this.upload_gambar_old.push({
              value: res.evidence.id,
              name: res.evidence.nama
            });
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          this.upload_gambar = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.upload_gambar = null;
        console.log(error);
      }
    );
  }

  // UPLOAD ============================================================================================================
  dropped(event: UploadEvent) {
    for (const droppedFile of event.files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // Here you can access the real file
          // console.log(droppedFile.relativePath, file);

          // You could upload it like this:
          const formData = new FormData()
          formData.append('files', file, droppedFile.relativePath)

          // Upload
          this.loading = true;
          this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
            result => {
              try {
                this.loading = false;
                const result_msg = JSON.parse(result._body);

                // Evidence
                const json_evidence = {
                  nama: result_msg[0].name,
                  path: result_msg[0].url,
                  user: '3',
                  dari: 'website',
                  is_public: false,
                  is_deleted: false,
                  created_at: new Date(),
                  updated_at: new Date()
                };
                this.loading = true;
                this.httpRequest.httpPost(this.url_evidence, json_evidence).subscribe(
                  result => {
                    try {
                      this.loading = false;
                      const result_msg = JSON.parse(result._body);
                      // this.fileUploads.push([
                      //   value => result_msg.id,
                      //   name => result_msg.nama,
                      // ]);
                      this.fileUploads.push({
                        value: result_msg.id,
                        name: result_msg.nama,
                        path: "/api" + result_msg.path
                      });
                    } catch (error) {
                      this.loading = false;
                      this.errorMessage.openErrorSwal('Something wrong.');
                      console.log(error);
                    }
                  },
                  error => {
                    this.loading = false;
                    console.log(error);
                  }
                );

              } catch (error) {
                this.loading = false;
                console.log(error);
              }
            },
            error => {
              this.loading = false;
              console.log(error);
            }
          );

        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  droppedMultiple(event) {
    for (var i = 0; i < event.target.files.length; i++) { 
      const formData = new FormData();
      formData.append("files", event.target.files[i]);

      // Upload
      this.loading = true;
      this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
        result => {
          try {
            this.loading = false;
            const result_msg = JSON.parse(result._body);

            // Evidence
            const json_evidence = {
              nama: result_msg[0].name,
              path: result_msg[0].url,
              user: '3',
              dari: 'website',
              is_public: false,
              is_deleted: false,
              created_at: new Date(),
              updated_at: new Date()
            };
            this.loading = true;
            this.httpRequest.httpPost(this.url_evidence, json_evidence).subscribe(
              result => {
                try {
                  this.loading = false;
                  const result_msg = JSON.parse(result._body);
                  // this.fileUploads.push([
                  //   value => result_msg.id,
                  //   name => result_msg.nama,
                  // ]);
                  this.fileUploads.push({
                    value: result_msg.id,
                    name: result_msg.nama,
                    path: "/api" + result_msg.path
                  });
                } catch (error) {
                  this.loading = false;
                  this.errorMessage.openErrorSwal('Something wrong.');
                  console.log(error);
                }
              },
              error => {
                this.loading = false;
                console.log(error);
              }
            );

          } catch (error) {
            this.loading = false;
            console.log(error);
          }
        },
        error => {
          this.loading = false;
          console.log(error);
        }
      );
    }
  }

  public fileOver(event){
    console.log(event);
  }
 
  public fileLeave(event){
    console.log(event);
  }

  // FILE SAYA =========================================================================================================
  getDataSaya() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    const id = sess['id'];
    const user = 'user=' + id;
    const start = '_start=' + this.currentSayaStart;
    const limit = '_limit=6';
    const sort = '_sort=id:desc';
    const filter = '?' + user + '&' + start + '&' + limit + '&' + sort;

    const json_saya = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_evidence + filter, json_saya).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);

          const newArray = result_msg.map(o => {
            this.fileSayas.push({
              value: o.id,
              name: o.nama,
              path: "/api" + o.path
            });
          });

          this.currentSayaStart = this.currentSayaStart + 6;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  loadmoreSaya(){
    this.getDataSaya();
  }

  // DYNAMIC FORM ======================================================================================================
  addFieldValueUploadGambar(id: number, nama: string, isChecked: boolean) {
    if(isChecked) {
      this.upload_gambar = this.myForm.get('upload_gambar') as FormArray;
      this.upload_gambar.push(this.createUploadGambar(id, nama));
    } else {
      this.upload_gambar.removeAt(this.upload_gambar.value.findIndex(res => res.value === id));
    }
  }

  createUploadGambar(id: number, nama: string): FormGroup {
    return new FormGroup({
      value: new FormControl({value: id, disabled: false}, []), 
      name: new FormControl({value: nama, disabled: false}, []), 
    });
  }

  removeFieldValueUploadGambar(i) {
    if(this.upload_gambar){
      this.upload_gambar.removeAt(i);
    }
    return false;
  }

  // DATEPICKER ========================================================================================================
  closeFix(event, datePicker) {
    if(event.target.offsetParent == null)
      datePicker.close();
    else if(event.target.offsetParent.nodeName != "NGB-DATEPICKER")
      datePicker.close();
  }

  // SAVE ==============================================================================================================
  onSubmit() {
    let id = this.route.snapshot.paramMap.get('id');

    this.submitted = true;
    let val_kegiatan = this.myForm.value;
    let val_gambar = this.myForm.value.upload_gambar;

    let tanggal = this.myForm.value.tanggal;
    let tanggal_asli = new Date(tanggal.year, tanggal.month - 1, tanggal.day);

    this.myForm.patchValue({
      is_deleted: false,
      // tahun: this.convert.formatDateY(tanggal_asli.toString()),
      // bulan: this.convert.formatDateM(tanggal_asli.toString()),
      // hari: this.convert.formatDateD(tanggal_asli.toString()),
      tanggal: this.convert.formatDateYMD(tanggal_asli.toString()),
      // tanggal_kegiatan: this.convert.formatDateYMD(tanggal_asli.toString()),
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    });

    val_kegiatan = this.myForm.value;
    delete val_kegiatan['upload_gambar'];

    // remove same value
    val_gambar = val_gambar.reduce((unique, o) => {
      if(!unique.some(obj => obj.value === o.value)) {
        unique.push(o);
      }
      return unique;
    },[]);

    let json_kegiatan = JSON.stringify(val_kegiatan);

    this.loading = true;
    this.httpRequest.httpPut(this.url_kegiatan + '/' + id, json_kegiatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          const id_kegiatan = result_msg.id;
          this.onSubmitEvidence(id_kegiatan, this.upload_gambar_old, val_gambar);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  async onSubmitEvidence(id_kegiatan, val_gambar_old, val_gambar) {

    var add = val_gambar.filter(function(obj) {
      return !val_gambar_old.some(function(obj2) {
        return obj.value == obj2.value;
      });
    });

    var remove = val_gambar_old.filter(function(obj) {
      return !val_gambar.some(function(obj2) {
        return obj.value == obj2.value;
      });
    });

    // add
    for(let res of add){
      let json_add = {
        kegiatan: id_kegiatan,
        evidence: res.value,
        is_deleted: false,
        created_at: new Date(),
        updated_at: new Date()
      };
      await this.httpRequest.httpPost(this.url_kegiatanevidence, json_add).toPromise()
      .then(
        result => {
          try {
            const result_msg_ke = JSON.parse(result._body);
          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
            console.log(error);
          }
        },
        error => {
          this.loading = false;
          console.log(error);
        }
      );
    }

    // delete
    for(let res of remove){
      // get kegiatanevidence
      const json_ke = {};
      this.loading = true;
      let ambil_kegiatanevidence = await this.httpRequest.httpGet(this.url_kegiatanevidence + '?kegiatan=' + id_kegiatan + '&evidence=' + res.value, json_ke).toPromise()
      .then(
        async result => {
          try {
            const result_msg = JSON.parse(result._body);
            return result_msg;
          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
            console.log(error);
          }
        },
        error => {
          this.loading = false;
          console.log(error);
        }
      );

      // update kegiatanevidence
      const id_kegiatanevidence = ambil_kegiatanevidence[0].id;
      let json_delete = {};
      let ubah_kegiatanevidence = await this.httpRequest.httpDelete(this.url_kegiatanevidence + '/' + id_kegiatanevidence, json_delete).toPromise()
      .then(
        result => {
          try {
            const result_msg_ke = JSON.parse(result._body);
          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
            console.log(error);
          }
        },
        error => {
          this.loading = false;
          console.log(error);
        }
      );
    }

    this.loading = false;
    await this.showSuccess();
  }

  showSuccess(){
    swal({
      title: 'Informasi',
      text: 'Kegiatan berhasil diubah.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/kegiatan/list'])
    });
  }

  // CLEAR GAMBAR ======================================================================================================
  clearUploadGambar(){

    this.fileUploads = [];

    this.fileSayas = [];
    this.currentSayaStart = 0;
    this.getDataSaya();

  }

}
