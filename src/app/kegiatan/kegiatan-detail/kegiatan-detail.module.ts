import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { KegiatanDetailComponent } from './kegiatan-detail.component';
import { NgxLoadingModule } from 'ngx-loading';

export const KegiatanDetailRoutes: Routes = [
  {
    path: '',
    component: KegiatanDetailComponent,
    data: {
      breadcrumb: 'Detail Kegiatan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(KegiatanDetailRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [KegiatanDetailComponent]
})
export class KegiatanDetailModule { }
