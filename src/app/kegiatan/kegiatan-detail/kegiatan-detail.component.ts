import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

@Component({
  selector: 'app-kegiatan-detail',
  templateUrl: './kegiatan-detail.component.html',
  styleUrls: ['../kegiatan.component.css'],
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class KegiatanDetailComponent implements OnInit {

  public loading = false;
  private url_kegiatan = '/api/kegiatan';
  private url_kegiatanevidence = '/api/kegiatanevidence';

  dataKegiatan = [];
  dataKegiatanEvidence = [];
  dataEvidence = [];

  images = [
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(145).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(145).jpg', description: 'Image 1' },
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(150).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(150).jpg', description: 'Image 2' },
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(152).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(152).jpg', description: 'Image 3' },
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(42).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(42).jpg', description: 'Image 4' },
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(151).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(151).jpg', description: 'Image 5' },
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(40).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(40).jpg', description: 'Image 6' },
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(148).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(148).jpg', description: 'Image 7' },
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(147).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(147).jpg', description: 'Image 8' },
    { img: 'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(149).jpg', thumb:
    'https://mdbootstrap.com/img/Photos/Lightbox/Original/img%20(149).jpg', description: 'Image 9' }
  ];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('kegiatan', 'detail')){
      this.router.navigate(['/error/403']);
    }
    
    let id = this.route.snapshot.paramMap.get('id');
    this.getDataKegiatan(id);
    this.getDataKegiatanEvidence(id);
  }

  getDataKegiatan(id) {
    const json_kegiatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatan + '/'+ id, json_kegiatan).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          this.dataKegiatan = result_msg;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          this.dataKegiatan = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataKegiatan = null;
        console.log(error);
      }
    );
  }

  getDataKegiatanEvidence(id) {
    const json_ke = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatanevidence + '?is_deleted=false&kegiatan='+ id, json_ke).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          this.dataKegiatanEvidence = result_msg;

          for(let res of result_msg){
            this.dataEvidence.push({
              value: res.evidence.id,
              name: res.evidence.nama,
              path: "/api" + res.evidence.path
            });
          }

          console.log(this.dataEvidence);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          this.dataKegiatanEvidence = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataKegiatanEvidence = null;
        console.log(error);
      }
    );
  }

}
