import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';
import { fadeInOutTranslate } from '../../shared/elements/animation';

import swal from 'sweetalert2';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

const now = new Date();

@Component({
  selector: 'app-kegiatan-add',
  templateUrl: './kegiatan-add.component.html',
  styleUrls: ['../kegiatan.component.css'],
  animations: [fadeInOutTranslate],
  encapsulation: ViewEncapsulation.None,
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class KegiatanAddComponent implements OnInit {

  public loading = false;
  private url_kegiatan = '/api/kegiatan';
  private url_user = '/api/users';
  private url_upload = '/api/upload'; 
  private url_evidence = '/api/evidence'; 
  private url_kegiatanevidence = '/api/kegiatanevidence'; 

  dataUser: any[] = [];
  files: UploadFile[] = [];

  myForm: FormGroup;

  upload_gambar: FormArray; 
  fileUploads: any[] = []; 
  fileSayas: any[] = []; 
  filePubliks: any[] = []; 

  submitted: boolean;

  currentSayaStart: number = 0;
  currentPublikStart: number = 0;

  tanggalMinPicker: NgbDateStruct = {
    year: now.getFullYear(), 
    month: now.getMonth() - 1, 
    day: now.getDate()
  };

  tanggalTodayPicker: NgbDateStruct = {
    year: now.getFullYear(), 
    month: now.getMonth() + 1, 
    day: now.getDate()
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('kegiatan','add')){
      this.router.navigate(['/error/403']);
    }

    // this.getDataUser();
    this.getDataSaya();
    this.getDataPublik();

    this.myForm = new FormGroup({
      user: new FormControl(),
      dinas: new FormControl(),
      bidang: new FormControl(),
      seksi: new FormControl(),
      // tahun: new FormControl(),
      // bulan: new FormControl(),
      // hari: new FormControl(),
      tanggal: new FormControl({value: this.tanggalTodayPicker, disabled: false}, [Validators.required]),
      // tanggal_kegiatan: new FormControl(),
      // tanggal_input: new FormControl(),
      nama_kegiatan: new FormControl({value: '', disabled: false}, [Validators.required]),
      tempat: new FormControl({value: '', disabled: false}, [Validators.required]),
      penyelenggara: new FormControl({value: '', disabled: false}, [Validators.required]),
      trigger: new FormControl(),
      is_tugas_pokok: new FormControl({value: '', disabled: false}, [Validators.required]),
      keterangan: new FormControl({value: '', disabled: false}, [Validators.required]),
      upload_gambar: new FormArray([]),
      is_deleted: new FormControl(),
      created_at: new FormControl(),
      updated_at: new FormControl(),
    },
      // this.passwordMatchValidator
    );  
  }

  // VALIDATION ========================================================================================================
  get tanggal() { return this.myForm.get('tanggal'); }
  get nama_kegiatan() { return this.myForm.get('nama_kegiatan'); }
  get tempat() { return this.myForm.get('tempat'); }
  get penyelenggara() { return this.myForm.get('penyelenggara'); }
  get is_tugas_pokok() { return this.myForm.get('is_tugas_pokok'); }
  get keterangan() { return this.myForm.get('keterangan'); }

  // USER ==============================================================================================================
  getDataUser() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    // const id = sess['id'];
    const id = 'me';

    const json_user = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_user + '/'+ id, json_user).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataUser = result_msg;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataUser = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataUser = null;
        console.log(error);
      }
    );
  }

  // DATEPICKER ========================================================================================================
  closeFix(event, datePicker) {
    if(event.target.offsetParent == null)
      datePicker.close();
    else if(event.target.offsetParent.nodeName != "NGB-DATEPICKER")
      datePicker.close();
  }

  // SAVE ==============================================================================================================
  onSubmit() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    this.submitted = true;
    let val_kegiatan = this.myForm.value;

    let tanggal = this.myForm.value.tanggal;
    let tanggal_asli = new Date(tanggal.year, tanggal.month - 1, tanggal.day);

    let val_gambar = this.myForm.value.upload_gambar;

    this.myForm.patchValue({
      user: sess['id'],
      dinas: sess['dinas'],
      bidang: sess['bidang'],
      seksi: sess['seksi'],
      // tahun: this.convert.formatDateY(tanggal_asli.toString()),
      // bulan: this.convert.formatDateM(tanggal_asli.toString()),
      // hari: this.convert.formatDateD(tanggal_asli.toString()),
      tanggal: this.convert.formatDateYMD(tanggal_asli.toString()),
      // tanggal_kegiatan: this.convert.formatDateYMD(tanggal_asli.toString()),
      // tanggal_input: this.convert.formatDateYMD(new Date().toString()),
      is_deleted: false,
      created_at: new Date(),
      updated_at: new Date()
      // created_at: this.convert.formatDateYMDHMS(new Date().toString()),
      // updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    });

    val_kegiatan = this.myForm.value;
    delete val_kegiatan['upload_gambar'];

    // remove same value
    val_gambar = val_gambar.reduce((unique, o) => {
      if(!unique.some(obj => obj.value === o.value)) {
        unique.push(o);
      }
      return unique;
    },[]);

    let json_kegiatan = JSON.stringify(val_kegiatan);

    this.loading = true;
    this.httpRequest.httpPost(this.url_kegiatan, json_kegiatan).subscribe(
      async result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          const id_kegiatan = result_msg.id;

          for(let res of val_gambar){
            // let cu = this.convert.formatDateYMDHMS(new Date().toString());
            let json_ke = {
              kegiatan: id_kegiatan,
              evidence: res.value,
              is_deleted: false,
              created_at: new Date(),
              updated_at: new Date()
            };
            await this.httpRequest.httpPost(this.url_kegiatanevidence, json_ke).subscribe(
              result => {
                try {
                  this.loading = false;
                  const result_msg_ke = JSON.parse(result._body);
                } catch (error) {
                  this.loading = false;
                  this.errorMessage.openErrorSwal('Something wrong.');
                  console.log(error);
                }
              },
              error => {
                this.loading = false;
                console.log(error);
              }
            );
          }

          this.showSuccess();

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(){
    swal({
      title: 'Informasi',
      text: 'Kegiatan berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/kegiatan/list'])
    });
  }

  dropped(event: UploadEvent) {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    for (const droppedFile of event.files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // Here you can access the real file
          // console.log(droppedFile.relativePath, file);

          // You could upload it like this:
          const formData = new FormData()
          formData.append('files', file, droppedFile.relativePath)

          // Upload
          this.loading = true;
          this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
            result => {
              try {
                this.loading = false;
                const result_msg = JSON.parse(result._body);

                // Evidence
                const json_evidence = {
                  nama: result_msg[0].name,
                  path: result_msg[0].url,
                  user: sess['id'],
                  dari: 'website',
                  is_public: false,
                  is_deleted: false,
                  // tahun: this.convert.formatDateY(new Date().toString()),
                  // bulan: this.convert.formatDateM(new Date().toString()),
                  // hari: this.convert.formatDateD(new Date().toString()),
                  // created_at: this.convert.formatDateYMDHMS(new Date().toString()),
                  // updated_at: this.convert.formatDateYMDHMS(new Date().toString())
                  created_at: new Date(),
                  updated_at: new Date()
                };
                this.loading = true;
                this.httpRequest.httpPost(this.url_evidence, json_evidence).subscribe(
                  result => {
                    try {
                      this.loading = false;
                      const result_msg = JSON.parse(result._body);
                      // this.fileUploads.push([
                      //   value => result_msg.id,
                      //   name => result_msg.nama,
                      // ]);
                      this.fileUploads.push({
                        value: result_msg.id,
                        name: result_msg.nama,
                        path: "/api" + result_msg.path
                      });
                    } catch (error) {
                      this.loading = false;
                      this.errorMessage.openErrorSwal('Something wrong.');
                      console.log(error);
                    }
                  },
                  error => {
                    this.loading = false;
                    console.log(error);
                  }
                );

              } catch (error) {
                this.loading = false;
                console.log(error);
              }
            },
            error => {
              this.loading = false;
              console.log(error);
            }
          );

        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  droppedMultiple(event) {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    for (var i = 0; i < event.target.files.length; i++) { 
      const formData = new FormData();
      formData.append("files", event.target.files[i]);

      // Upload
      this.loading = true;
      this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
        result => {
          try {
            this.loading = false;
            const result_msg = JSON.parse(result._body);

            // Evidence
            const json_evidence = {
              nama: result_msg[0].name,
              path: result_msg[0].url,
              user: sess['id'],
              dari: 'website',
              is_public: false,
              is_deleted: false,
              // tahun: this.convert.formatDateY(new Date().toString()),
              // bulan: this.convert.formatDateM(new Date().toString()),
              // hari: this.convert.formatDateD(new Date().toString()),
              // created_at: this.convert.formatDateYMDHMS(new Date().toString()),
              // updated_at: this.convert.formatDateYMDHMS(new Date().toString())
              created_at: new Date(),
              updated_at: new Date()
            };
            this.loading = true;
            this.httpRequest.httpPost(this.url_evidence, json_evidence).subscribe(
              result => {
                try {
                  this.loading = false;
                  const result_msg = JSON.parse(result._body);
                  // this.fileUploads.push([
                  //   value => result_msg.id,
                  //   name => result_msg.nama,
                  // ]);
                  this.fileUploads.push({
                    value: result_msg.id,
                    name: result_msg.nama,
                    path: "/api" + result_msg.path
                  });
                } catch (error) {
                  this.loading = false;
                  this.errorMessage.openErrorSwal('Something wrong.');
                  console.log(error);
                }
              },
              error => {
                this.loading = false;
                console.log(error);
              }
            );

          } catch (error) {
            this.loading = false;
            console.log(error);
          }
        },
        error => {
          this.loading = false;
          console.log(error);
        }
      );
    }
  }

  public fileOver(event){
    console.log(event);
  }
 
  public fileLeave(event){
    console.log(event);
  }

  // EVIDENCE ==========================================================================================================
  getDataEvidence() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    const id = sess['id'];

    const json_evidence = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_evidence + '/'+ id, json_evidence).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);

          this.upload_gambar = this.myForm.get('upload_gambar') as FormArray;
          this.upload_gambar.push(this.createUploadGambar(result_msg.id, result_msg.nama));

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  // FILE SAYA =========================================================================================================
  getDataSaya() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    const id = sess['id'];
    const user = 'user=' + id;
    const dari = 'dari=website';
    const start = '_start=' + this.currentSayaStart;
    const limit = '_limit=8';
    const sort = '_sort=id:desc';
    const filter = '?' + user + '&' + dari + '&' + start + '&' + limit + '&' + sort;

    const json_saya = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_evidence + filter, json_saya).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          const jumlah = result_msg.length;

          const newArray = result_msg.map(o => {
            this.fileSayas.push({
              value: o.id,
              name: o.nama,
              tanggal: this.convert.formatDateIndoTanggal(o.created_at),
              path: "/api" + o.path
            });
          });

          if(jumlah > 0){
            this.currentPublikStart = this.currentPublikStart + jumlah;
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  loadmoreSaya(){
    this.getDataSaya();
  }

  // FILE PUBLIK
  getDataPublik() {
    const publik = 'is_public=true';
    const start = '_start=' + this.currentPublikStart;
    const limit = '_limit=8';
    const sort = '_sort=created_at:desc';
    const filter = '?' + publik + '&' + start + '&' + limit + '&' + sort;

    const json_saya = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_evidence + filter, json_saya).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          const jumlah = result_msg.length;

          const newArray = result_msg.map(o => {

            var path = "/api" + o.path;
            
            // if(o.dari == 'website'){
            //   var path = "/api" + o.path;
            // } else if(o.dari == 'telegram'){
            //   var path = "https://api.telegram.org/file/bot" + o.keterangan + o.path;
            // }

            this.filePubliks.push({
              value: o.id,
              name: o.nama,
              nama_telegram: o.nama_telegram,
              tanggal: this.convert.formatDateIndoTanggal(o.created_at),
              tag: o.tag,
              path: path
            });
          });

          if(jumlah > 0){
            this.currentPublikStart = this.currentPublikStart + jumlah;
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  loadmorePublik(){
    this.getDataPublik();
  }

  // DYNAMIC FORM ======================================================================================================
  addFieldValueUploadGambar(id: number, nama: string, isChecked: boolean) {
    if(isChecked) {
      this.upload_gambar = this.myForm.get('upload_gambar') as FormArray;
      this.upload_gambar.push(this.createUploadGambar(id, nama));
    } else {
      this.upload_gambar.removeAt(this.upload_gambar.value.findIndex(res => res.value === id));
    }
  }

  createUploadGambar(id: number, nama: string): FormGroup {
    return new FormGroup({
      value: new FormControl({value: id, disabled: false}, []), 
      name: new FormControl({value: nama, disabled: false}, []), 
    });
  }

  removeFieldValueUploadGambar(i) {
    if(this.upload_gambar){
      this.upload_gambar.removeAt(i);
    }
    return false;
  }

  // CLEAR GAMBAR ======================================================================================================
  clearUploadGambar(){
    this.fileUploads = [];

    this.fileSayas = [];
    this.currentSayaStart = 0;
    this.getDataSaya();

    this.filePubliks = [];
    this.currentPublikStart = 0;
    this.getDataPublik();
  }

}
