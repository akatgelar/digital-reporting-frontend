import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { KegiatanAddComponent } from './kegiatan-add.component';
import { FileDropModule } from 'ngx-file-drop';
import { NgxLoadingModule } from 'ngx-loading';

export const KegiatanAddRoutes: Routes = [
  {
    path: '',
    component: KegiatanAddComponent,
    data: {
      breadcrumb: 'Tambah Kegiatan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(KegiatanAddRoutes),
    SharedModule,
    FileDropModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [KegiatanAddComponent]
})
export class KegiatanAddModule { }
