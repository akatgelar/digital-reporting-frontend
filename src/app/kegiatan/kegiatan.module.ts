import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { KegiatanRoutes } from './kegiatan.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KegiatanRoutes),
      SharedModule
  ],
  declarations: []
})

export class KegiatanModule {}
