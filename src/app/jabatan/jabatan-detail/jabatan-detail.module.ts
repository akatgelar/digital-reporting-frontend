import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { JabatanDetailComponent } from './jabatan-detail.component';
import { NgxLoadingModule } from 'ngx-loading';

export const JabatanDetailRoutes: Routes = [
  {
    path: '',
    component: JabatanDetailComponent,
    data: {
      breadcrumb: 'Detail Jabatan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(JabatanDetailRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [JabatanDetailComponent]
})
export class JabatanDetailModule { }
