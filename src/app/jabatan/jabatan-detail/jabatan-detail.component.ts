import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

@Component({
  selector: 'app-jabatan-detail',
  templateUrl: './jabatan-detail.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class JabatanDetailComponent implements OnInit {

  public loading = false;
  private url_jabatan = '/api/jabatan';

  dataJabatan = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('jabatan','detail')){
      this.router.navigate(['/error/403']);
    }

    let id = this.route.snapshot.paramMap.get('id');
    this.getDataJabatan(id);
  }

  getDataJabatan(id) {
    const json_jabatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_jabatan + '/'+ id, json_jabatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataJabatan = result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataJabatan = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataJabatan = null;
        console.log(error);
      }
    );
  }

}
