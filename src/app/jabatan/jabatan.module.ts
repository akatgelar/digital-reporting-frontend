import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { JabatanRoutes } from './jabatan.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(JabatanRoutes),
      SharedModule
  ],
  declarations: []
})

export class JabatanModule {}
