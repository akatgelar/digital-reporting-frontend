import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { JabatanEditComponent } from './jabatan-edit.component';
import { NgxLoadingModule } from 'ngx-loading';

export const JabatanEditRoutes: Routes = [
  {
    path: '',
    component: JabatanEditComponent,
    data: {
      breadcrumb: 'Ubah Jabatan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(JabatanEditRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [JabatanEditComponent]
})
export class JabatanEditModule { }
