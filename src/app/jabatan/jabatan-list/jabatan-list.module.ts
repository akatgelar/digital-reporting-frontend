import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { JabatanListComponent } from './jabatan-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const JabatanListRoutes: Routes = [
  {
    path: '',
    component: JabatanListComponent,
    data: {
      breadcrumb: 'List Jabatan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(JabatanListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [JabatanListComponent]
})
export class JabatanListModule { }
