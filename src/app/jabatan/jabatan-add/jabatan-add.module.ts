import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { JabatanAddComponent } from './jabatan-add.component';
import { NgxLoadingModule } from 'ngx-loading';

export const JabatanAddRoutes: Routes = [
  {
    path: '',
    component: JabatanAddComponent,
    data: {
      breadcrumb: 'Tambah Jabatan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(JabatanAddRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [JabatanAddComponent]
})
export class JabatanAddModule { }
