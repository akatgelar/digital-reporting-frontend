import { Routes } from '@angular/router';

export const JabatanRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Jabatan',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './jabatan-list/jabatan-list.module#JabatanListModule'
      },
      {
        path: 'add',
        loadChildren: './jabatan-add/jabatan-add.module#JabatanAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './jabatan-detail/jabatan-detail.module#JabatanDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './jabatan-edit/jabatan-edit.module#JabatanEditModule'
      }
    ]
  }
]
