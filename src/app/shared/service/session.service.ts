import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { MenuItemsAdmin } from '../../shared/menu-items/menu-items-admin';
import { MenuItemsPegawai } from '../../shared/menu-items/menu-items-pegawai';
// import { MenuItemsPelapor } from '../../shared/menu-items/menu-items-pelapor';
// import { MenuItemsVerifikator } from '../../shared/menu-items/menu-items-verifikator';
// import { MenuItemsPemantau } from '../../shared/menu-items/menu-items-pemantau';
import { AccessRole } from '../../shared/menu-items/access-role';

@Injectable()
export class SessionService {

  constructor(
    private accessRole: AccessRole,
    private menuItems: MenuItems,
    private menuItemsAdmin: MenuItemsAdmin,
    private menuItemsPegawai: MenuItemsPegawai,
  ) {}

  setToken(token: string) {
    localStorage.setItem('digital-reporting-token', token);
  }

  getToken() {
    return localStorage.getItem('digital-reporting-token');
  }

  setData(data: string) {
    localStorage.setItem('digital-reporting-data', data);
  }

  getData() {
    return localStorage.getItem('digital-reporting-data');
  }

  setMenu(menu: string) {
    localStorage.setItem('digital-reporting-menu', menu);
  }

  getMenu() {
    return JSON.parse(localStorage.getItem('digital-reporting-menu'));
  }

  getRole() {
    const data = this.getData();
    const result_msg = JSON.parse(data);
    try {
      let role = result_msg['role']['type'];
      role = role.toString().toLowerCase();
      role = role.replace('_', '-');
      return role;
    } catch (error) {
      return null;
    }
  }

  getTokenExpired = function() {
    const token = this.getToken();
    const jwtHelper = new JwtHelperService();

    try {
      const date = jwtHelper.getTokenExpirationDate(token);
      return date;
    } catch (error) {
      return null;
    }
  };

  isLoggedIn = function() {
    const token = this.getToken();
    const jwtHelper = new JwtHelperService();

    if (token) {
      const payload = jwtHelper.decodeToken(token);
      const date = jwtHelper.getTokenExpirationDate(token);
      const expired = jwtHelper.isTokenExpired(token);

      if (expired) {
        return false;
      }else {
        return true;
      }

    } else {
      return false;
    }
  };

  checkMenu(route1, route2) {
    if(route1 === ''){
      route1 = 'dashboard';
    }if(route1 === 'profile'){
      route1 = 'dashboard';
    }

    let role = this.getRole();
    let role_access = [];
    let role_access_find = {};
    let role_access_can = false;

    if(role === 'admin'){
      role_access = this.menuItemsAdmin.getAll();
    } else if(role === 'pegawai'){
      role_access = this.menuItemsPegawai.getAll();
    } 

    try {
      for (var a=0; a < role_access.length; a++) {
        for (var i=0; i < role_access[a]['main'].length; i++) {
          if (role_access[a]['main'][i].state === route1) {
            role_access_find = role_access[a]['main'][i];
            role_access_can = role_access_find['display'];
          }
        }
      }
      return role_access_can;

    } catch (error) {
      console.log("catch")
      return false;
    }
  }

  checkAccess(controller, func) {
    let accessRole = this.accessRole.getAll();
    let roles = this.getRole();
    // if(roles === 'dinas'){
    //   roles = 'dinass';
    // }
    let controllers = controller;
    let funcs = func;

    let can_access = false;
    try {
      can_access = accessRole[roles][controllers][funcs];
    } catch (error) {
      can_access = false;
    }

    return can_access;
  }

  checkAccessSession(route1, route2) {
    let result_msg = JSON.parse(this.getData());
    console.log(result_msg)
    let route3 = '';

    if(route1 === ''){
      route1 = 'dashboard';
    }

    if(route2 === 'list') {
      route3 = 'find';
    }else if(route2 === 'detail'){
      route3 = 'findone';
    }else if(route2 === 'statistic'){
      route3 = 'findone';
    }else if(route2 === 'add'){
      route3 = 'create';
    }else if(route2 === 'edit'){
      route3 = 'update';
    }else if(route2 === 'delete'){
      route3 = 'delete';
    }else{
      route3 = 'find';
    }

    try {
      if(route1 === 'dashboard' || route1 ==='logout' || route1 === 'profile'){
        return true;
      }
      else if(route1 === 'user'){
        return result_msg['role']['permissions']['users-permissions']['controllers']['user']['find']['enabled'];
      }
      else{
        return result_msg['role']['permissions']['application']['controllers'][route1][route3]['enabled'];
      }

    } catch (error) {
      console.log("catch")
      return false;
    }
  }

  logIn = function(token, data, menu) {
    this.setData(data);
    this.setToken(token);
    this.setMenu(menu);
    return this.getToken();
  };

  logOut = function() {
    localStorage.removeItem('digital-reporting-token');
    localStorage.removeItem('digital-reporting-data');
    localStorage.removeItem('digital-reporting-menu');
  };

}
