import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class ConverterService {

  constructor() {
  }

  formatDateIndoTglJam(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('DD MMMM YYYY hh:mm:ss') ;
      return new_date;
    }else {
      return null;
    }
  }

  formatDateIndoTanggal(date: string) {
    var hari = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    var bulan = ['Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

    var tanggal = new Date(date).getDate();
    var xhari = new Date(date).getDay();
    var xbulan = new Date(date).getMonth();
    var xtahun = new Date(date).getFullYear();

    var h_hari = hari[xhari];
    var h_bulan = bulan[xbulan];
    var h_tahun = (xtahun < 1000) ? xtahun + 1900 : xtahun;

    return h_hari +', ' + tanggal + ' ' + h_bulan + ' ' + h_tahun;
  }

  formatDateIndoTgl(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('DD MMMM YYYY') ;
      return new_date;
    }else {
      return null;
    }
  }

  formatDateDMY(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('DD-MM-YYYY') ;
      return new_date;
    }else {
      return null;
    }
  }

  formatDateY(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('YYYY') ;
      return new_date;
    }else {
      return null;
    }
  }

  formatDateM(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('MM') ;
      return new_date;
    }else {
      return null;
    }
  }

  formatDateD(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('DD') ;
      return new_date;
    }else {
      return null;
    }
  }

  formatDateYMD(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('YYYY-MM-DD') ;
      return new_date;
    }else {
      return null;
    }
  }

  formatDateYMDHMS(date: string) {
    if (date) {
      moment.locale('id');
      const new_date = moment(date).format('YYYY-MM-DD HH:mm:ss') ;
      return new_date;
    }else {
      return null;
    }
  }

  formatRupiah(angka: string) {
    if (angka) {
      const prefix = 'Rp. ';
      const number_string = angka.toString().replace(/[^,\d]/g, '');
      const split = number_string.split(',');
      const sisa = split[0].length % 3;
      const ribuan = split[0].substr(sisa).match(/\d{3}/gi);
      let rupiah = split[0].substr(0, sisa);

      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if (ribuan) {
        const separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }

      rupiah = split[1] !== undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix === undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
  }


}
