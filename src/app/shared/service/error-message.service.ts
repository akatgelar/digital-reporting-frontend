import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Injectable()

export class ErrorMessageService {

    constructor(private router: Router) { }

    // error message action
    errorserver() {
        swal(
            'Infomation!',
            'Can\'t connect to server.',
            'error'
        );
    }

    // not found
    notfound() {
        swal(
            'Infomation!',
            'Data not found.',
            'error'
        );
    }

    // success notification
    openSuccessSwal(message) {
        swal({
            title: 'Information!',
            html: message,
            type: 'success',
            allowOutsideClick: false
        }).catch(swal.noop);
    }

    // success notification
    openErrorSwal(message) {
        if(message === 'Email is already taken.'){
            message = "Username sudah dipakai."
        }
        swal({
            title: 'Information!',
            html: message,
            type: 'error',
            allowOutsideClick: false
        }).catch(swal.noop);
    }



}
