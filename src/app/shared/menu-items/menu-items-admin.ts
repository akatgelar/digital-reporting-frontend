import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Navigation',
    display: false,
    main: [
      {
        state: 'dashboard',
        short_label: 'D',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard',
        display: true
      },
      {
        state: 'kegiatan',
        short_label: 'K',
        name: 'Kegiatan',
        type: 'link',
        icon: 'ti-files',
        display: true
      },
      {
        state: 'evidence',
        short_label: 'E',
        name: 'Evidence',
        type: 'link',
        icon: 'ti-image',
        display: true
      },
      {
        state: 'user',
        short_label: 'U',
        name: 'User',
        type: 'link',
        icon: 'ti-user',
        display: true
      },
      {
        state: 'laporan',
        short_label: 'L',
        name: 'Laporan',
        type: 'link',
        icon: 'ti-agenda',
        display: true
      }
    ]
  },
  {
    label: 'Master',
    display: true,
    main: [
      {
        state: 'dinas',
        short_label: 'D',
        name: 'Dinas',
        type: 'link',
        icon: 'ti-layout-grid2-alt',
        display: true
      },
      {
        state: 'bidang',
        short_label: 'B',
        name: 'Bidang',
        type: 'link',
        icon: 'ti-layout-grid3-alt',
        display: true
      },
      {
        state: 'seksi',
        short_label: 'S',
        name: 'Seksi',
        type: 'link',
        icon: 'ti-layout-grid4-alt',
        display: true
      },
      {
        state: 'jabatan',
        short_label: 'J',
        name: 'Jabatan',
        type: 'link',
        icon: 'ti-flag',
        display: true
      },
      {
        state: 'pangkat',
        short_label: 'p',
        name: 'Pangkat',
        type: 'link',
        icon: 'ti-flag-alt',
        display: true
      },
      {
        state: 'template',
        short_label: 't',
        name: 'Template',
        type: 'link',
        icon: 'ti-clipboard',
        display: true
      }
    ]
  },
];

@Injectable()
export class MenuItemsAdmin {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  getAlls(): Menu[]{
    console.log(JSON.parse(localStorage.getItem('digital-reporting-menu')));
    return JSON.parse(localStorage.getItem('digital-reporting-menu'));
  }
}
