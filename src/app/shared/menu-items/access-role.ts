import {Injectable} from '@angular/core';

const access_role = 
{ 
  admin: {
    dashboard: {
      list: true
    },
    profile: {
      list: true,
      edit: true
    },
    user: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    jabatan: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    pangkat: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    template: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    dinas: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    bidang: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    seksi: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    evidence: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    kegiatan: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    laporan: {
      list: true
    },
  },
  pegawai: {
    dashboard: {
      list: true
    },
    profile: {
      list: true,
      edit: true
    },
    user: {
      list: false,
      detail: false,
      add: false,
      edit: false,
      delete: false
    },
    jabatan: {
      list: false,
      detail: false,
      add: false,
      edit: false,
      delete: false
    },
    pangkat: {
      list: false,
      detail: false,
      add: false,
      edit: false,
      delete: false
    },
    template: {
      list: false,
      detail: false,
      add: false,
      edit: false,
      delete: false
    },
    dinas: {
      list: false,
      detail: false,
      add: false,
      edit: false,
      delete: false
    },
    bidang: {
      list: false,
      detail: false,
      add: false,
      edit: false,
      delete: false
    },
    seksi: {
      list: false,
      detail: false,
      add: false,
      edit: false,
      delete: false
    },
    evidence: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    kegiatan: {
      list: true,
      detail: true,
      add: true,
      edit: true,
      delete: true
    },
    laporan: {
      list: true
    },
  }
};

@Injectable()
export class AccessRole {
  getAll(){
    return access_role;
  }
}
