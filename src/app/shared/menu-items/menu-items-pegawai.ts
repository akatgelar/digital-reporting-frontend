import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Navigation',
    display: false,
    main: [
      {
        state: 'dashboard',
        short_label: 'D',
        name: 'Dashboard',
        type: 'link',
        icon: 'ti-dashboard',
        display: true
      },
      {
        state: 'kegiatan',
        short_label: 'K',
        name: 'Kegiatan',
        type: 'link',
        icon: 'ti-files',
        display: true
      },
      {
        state: 'evidence',
        short_label: 'E',
        name: 'Evidence',
        type: 'link',
        icon: 'ti-image',
        display: true
      },
      {
        state: 'laporan',
        short_label: 'L',
        name: 'Laporan',
        type: 'link',
        icon: 'ti-agenda',
        display: true
      }
    ]
  },
];

@Injectable()
export class MenuItemsPegawai {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  getAlls(): Menu[]{
    console.log(JSON.parse(localStorage.getItem('digital-reporting-menu')));
    return JSON.parse(localStorage.getItem('digital-reporting-menu'));
  }
}
