import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import {
  AsyncValidatorFn,
  AbstractControl,
  ValidationErrors
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { HttpRequestService } from '../shared/service/http-request.service';

@Injectable()
export class UsernameValidationService {

  private url_check_username = '/api/users/mecheckusernameupdate'; 

  un: boolean;

  takenUsernames = ['hello', 'world', 'username'];

  constructor(
    private http: HttpClient,
    private httpRequest: HttpRequestService,
  ) {}

  checkIfUsernameExists(un: string): Observable<boolean> {
    const is_deleted = 'is_deleted=false';
    const username = 'username=' + un;
    const filter = '?' + is_deleted + '&' + username;
    const json_user = {};

    const check = this.httpRequest.httpGet(this.url_check_username + filter, json_user).subscribe(
      (result:any) => {
        const result_msg = JSON.parse(result._body);
        // this.un = result_msg.check;
        console.log('a', result_msg.check);
        return result_msg.check;
        // return Observable.of(result_msg.check).pipe(delay(1000));
      });

    console.log('b', Observable.of(check).pipe(delay(1000)));

    // console.log(Observable.of(this.http.get(this.url_check_username + filter, json_user)));
    // console.log(Observable.of(Observable.of(check)));

    // return Observable.of(this.http.get(this.url_check_username + filter, json_user)).pipe(delay(1000));
    return Observable.of(false).pipe(delay(1000));
  }

  usernameValidator(): AsyncValidatorFn {
    return (control: AbstractControl): Observable<ValidationErrors | null> => {
      return this.checkIfUsernameExists(control.value).pipe(
        map(res => {
          console.log(res);
          return res ? { usernameExists: true } : null;
        })
      );
    };
  }
}
