import { Routes } from '@angular/router';

export const ProfileRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Profile',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './profile-list/profile-list.module#ProfileListModule'
      },
      {
        path: 'edit',
        loadChildren: './profile-edit/profile-edit.module#ProfileEditModule'
      }
    ]
  }
]
