import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { ProfileRoutes } from './profile.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ProfileRoutes),
      SharedModule
  ],
  declarations: []
})

export class ProfileModule {}
