import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { ProfileEditComponent } from './profile-edit.component';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';

export const ProfileEditRoutes: Routes = [
  {
    path: '',
    component: ProfileEditComponent,
    data: {
      breadcrumb: 'Ubah Profile',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProfileEditRoutes),
    SharedModule,
    ShowHidePasswordModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [ProfileEditComponent]
})
export class ProfileEditModule { }
