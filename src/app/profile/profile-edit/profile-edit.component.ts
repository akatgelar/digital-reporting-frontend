import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';
// import { UsernameValidationService } from '../usernames.service';

import swal from 'sweetalert2';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { IOption } from "ng-select";

const now = new Date();

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class ProfileEditComponent implements OnInit {

  public loading = false;
  private url_user = '/api/users/me'; 
  private url_users = '/api/users'; 
  private url_check_username = '/api/users/mecheckusernameupdate'; 

  private validationUsername: number;

  dataUser = [];  
  usernameUser = [];  

  myForm: FormGroup;
  submitted: boolean;

  lahirMinPicker: NgbDateStruct = {
    year: now.getFullYear() - 75,
    month: now.getMonth() + 1,
    day: now.getDate()
  };

  lahirMaxPicker: NgbDateStruct = {
    year: now.getFullYear(),
    month: now.getMonth() + 1,
    day: now.getDate()
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService,
      private http: Http,
  ) { }

  public jwt = this.session.getToken();
  public header = { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.jwt };

  ngOnInit() {
    if(!this.session.checkAccess('profile', 'list')){
      this.router.navigate(['/error/403']);
    }

    this.getDataUser();

    this.myForm = new FormGroup({
      nama: new FormControl({value: '', disabled: false}, [Validators.required]),
      jenis_kelamin: new FormControl({value: '', disabled: false}, [Validators.required]),
      tempat_lahir: new FormControl({value: '', disabled: false}, [Validators.required]),
      tanggal_lahir: new FormControl({value: this.lahirMaxPicker, disabled: false}, [Validators.required]),
      alamat: new FormControl({value: '', disabled: false}, [Validators.required]),
      no_telp: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[0-9]*$")]),
      email: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      username: new FormControl({value: '', disabled: false}, [Validators.required, Validators.minLength(5)]),
      password: new FormControl({value: '', disabled: false}, [Validators.minLength(6)]),
      updated_at: new FormControl(),
    });
  }

  get nama() { return this.myForm.get('nama'); }
  get jenis_kelamin() { return this.myForm.get('jenis_kelamin'); }
  get tempat_lahir() { return this.myForm.get('tempat_lahir'); }
  get tanggal_lahir() { return this.myForm.get('tanggal_lahir'); }
  get alamat() { return this.myForm.get('alamat'); }
  get no_telp() { return this.myForm.get('no_telp'); }
  get email() { return this.myForm.get('email'); }
  get username() { return this.myForm.get('username'); }
  get password() { return this.myForm.get('password'); }

  // valid(control: AbstractControl){
  //   // return { usernameExists: true };

  //   const filter = '?' + 'username=' + control.value;
  //   const json_user = {};

  //   this.http.get( this.url_check_username, { headers : new Headers(this.header) })
  //     .map(result => {
  //         // console.log('httpGet result');
  //         console.log(result.json());
  //         // return Observable.of(result).delay(500);;
  //         return result.json();
  //     });

  //   var cek = this.httpRequest.httpGetJSON(this.url_check_username + filter, json_user);
    // console.log(cek);
    // this.httpRequest.httpGetJSON(this.url_check_username + filter, json_user).subscribe(
    //   result => {
    //     try {
    //       const result_msg = JSON.parse(result._body);
    //       console.log(result_msg.check);
    //       if(result_msg.check == true){
    //         return { usernameExists: true };
    //       } else {
    //         return null;
    //       }
    //     } catch (error) {
    //       this.errorMessage.openErrorSwal('Something wrong.');
    //       return null;
    //     }
    //   },
    //   error => {
    //     console.log(error);
    //     return null;
    //   }
    // );
  // }
 
  getDataUser() {
    const json_user = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_user , json_user).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.dataUser = result_msg;
          this.setForm(this.dataUser);
          this.loading = false;
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  setForm(data){
    let tanggal = new Date(data.tanggal_lahir);
    let lahirPicker = {
      year: tanggal.getFullYear(),
      month: tanggal.getMonth() + 1,
      day: tanggal.getDate()
    };

    this.myForm.patchValue({
      nama: data.nama,
      jenis_kelamin: data.jenis_kelamin,
      tempat_lahir: data.tempat_lahir,
      tanggal_lahir: lahirPicker,
      alamat: data.alamat,
      no_telp: data.no_telp,
      keterangan: data.keterangan,
      email: data.email,
      username: data.username
    });
  }

  // DATEPICKER ========================================================================================================
  closeFix(event, datePicker) {
    if(event.target.offsetParent == null)
      datePicker.close();
    else if(event.target.offsetParent.nodeName != "NGB-DATEPICKER")
      datePicker.close();
  }

  // SAVE
  onSubmit() {
    //SESSION
    const sess = JSON.parse(this.session.getData());

    this.submitted = true;
    let val_user = this.myForm.value;

    let tanggal = this.myForm.value.tanggal_lahir;

    this.myForm.patchValue({
      tanggal_lahir: this.convert.formatDateYMD(new Date(tanggal.year, tanggal.month - 1, tanggal.day).toString()),
      updated_at: new Date()
    });

    val_user = this.myForm.value;

    if(val_user['password'] === ''){
      delete val_user['password'];
    }

    // console.log(val_user['username']);
    // console.log(this.dataUser['username']);

    // if(val_user['username'].toString() == this.dataUser['username'].toString()){
    //   delete val_user['username'];
    // }

    let json_user = JSON.stringify(val_user);

    this.loading = true;
    this.httpRequest.httpPut(this.url_users + '/' + sess['id'], json_user).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.showSuccess(result_msg['nama']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'Profile ' + nama + ' berhasil diubah.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/profile/list'])
    });
  }
}
