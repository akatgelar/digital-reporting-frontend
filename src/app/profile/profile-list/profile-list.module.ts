import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { ProfileListComponent } from './profile-list.component';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';

export const ProfileRoutes: Routes = [
  {
    path: '',
    component: ProfileListComponent,
    data: {
      breadcrumb: 'Profile',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProfileRoutes),
    SharedModule,
    ShowHidePasswordModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [ProfileListComponent]
})
export class ProfileListModule { }
