import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { IOption } from "ng-select";

const now = new Date();

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class ProfileListComponent implements OnInit {

  public loading = false;
  private url_user = '/api/users/me'; 
  private url_users = '/api/users'; 

  editProfile = false;

  dataUser = [];  
  dataRole = [];
  dataDinas = [];
  dataBidang = [];
  dataSeksi = [];
  dataJabatan = [];
  dataPangkat = [];

  myForm: FormGroup;
  submitted: boolean;

  lahirMinPicker: NgbDateStruct = {
    year: now.getFullYear() - 75,
    month: now.getMonth() + 1,
    day: now.getDate()
  };

  lahirMaxPicker: NgbDateStruct = {
    year: now.getFullYear(),
    month: now.getMonth() + 1,
    day: now.getDate()
  };

  constructor(
    private router: Router,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('profile', 'list')){
      this.router.navigate(['/error/403']);
    }

    this.getDataUser();

    // if(this.editProfile){
    //   this.myForm = new FormGroup({
    //     nama: new FormControl({value: '', disabled: false}, [Validators.required]),
    //     jenis_kelamin: new FormControl({value: '', disabled: false}, [Validators.required]),
    //     tempat_lahir: new FormControl({value: '', disabled: false}, [Validators.required]),
    //     tanggal_lahir: new FormControl({value: this.lahirMaxPicker, disabled: false}, [Validators.required]),
    //     alamat: new FormControl({value: '', disabled: false}, [Validators.required]),
    //     no_telp: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[0-9]*$")]),
    //     email: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
    //     username: new FormControl({value: '', disabled: false}, [Validators.required, Validators.minLength(5), this.validateUsername.bind(this)]),
    //     password: new FormControl({value: '', disabled: false}, [Validators.minLength(6)]),
    //     updated_at: new FormControl(),
    //   });
    // }
  }

  // get nama() { return this.myForm.get('nama'); }
  // get jenis_kelamin() { return this.myForm.get('jenis_kelamin'); }
  // get tempat_lahir() { return this.myForm.get('tempat_lahir'); }
  // get tanggal_lahir() { return this.myForm.get('tanggal_lahir'); }
  // get alamat() { return this.myForm.get('alamat'); }
  // get no_telp() { return this.myForm.get('no_telp'); }
  // get email() { return this.myForm.get('email'); }
  // get username() { return this.myForm.get('username'); }
  // get password() { return this.myForm.get('password'); }

  // private async validateUsername(control: AbstractControl) {
  //   const val = control.value;

  //   const is_deleted = 'is_deleted=false';
  //   const username_ne = 'username_ne=' + this.dataUser['username'];
  //   const username = 'username=' + val;
  //   const filter = '?' + is_deleted + '&' + username_ne + '&' + username;

  //   const json_user = {};
  //   const cek = await this.httpRequest.httpGet(this.url_users + filter, json_user).toPromise().then(
  //     result => {
  //       try {
  //         const result_msg = JSON.parse(result._body);
  //         return (result_msg.length > 0) ? { alreadyExist: true } : null;
  //         // return { alreadyExist: true };
  //       } catch (error) {
  //         this.errorMessage.openErrorSwal('Something wrong.');
  //       }
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );

  //   console.log(cek)

  //   // return cek;
  //   return { alreadyExist: true };
  // }
 
  getDataUser() {
    const json_user = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_user , json_user).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.dataUser = result_msg;
          this.dataDinas = result_msg.dinas;
          this.dataBidang = result_msg.bidang;
          this.dataSeksi = result_msg.seksi;
          this.dataJabatan = result_msg.jabatan;
          this.dataPangkat = result_msg.pangkat;
          this.dataRole = result_msg.role;
          // this.setForm(this.dataUser);
          this.loading = false;
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  // setForm(data){
  //   let tanggal = new Date(data.tanggal_lahir);
  //   let lahirPicker = {
  //     year: tanggal.getFullYear(),
  //     month: tanggal.getMonth() + 1,
  //     day: tanggal.getDate()
  //   };

  //   this.myForm.patchValue({
  //     nama: data.nama,
  //     jenis_kelamin: data.jenis_kelamin,
  //     tempat_lahir: data.tempat_lahir,
  //     tanggal_lahir: lahirPicker,
  //     alamat: data.alamat,
  //     no_telp: data.no_telp,
  //     keterangan: data.keterangan,
  //     email: data.email,
  //     username: data.username
  //   });
  // }

  // SAVE
  // onSubmit() {
  //   this.submitted = true;
  //   let val_user = this.myForm.value;

  //   let tanggal = this.myForm.value.tanggal_lahir;

  //   this.myForm.patchValue({
  //     tanggal_lahir: this.convert.formatDateYMD(new Date(tanggal.year, tanggal.month - 1, tanggal.day).toString()),
  //     updated_at: new Date()
  //   });

  //   val_user = this.myForm.value;

  //   if(val_user['password'] === ''){
  //     delete val_user['password'];
  //   }

  //   console.log(val_user['username']);
  //   console.log(this.dataUser['username']);

  //   if(val_user['username'] === this.dataUser['username']){
  //     delete val_user['username'];
  //   }

  //   let json_user = JSON.stringify(val_user);

  //   this.loading = true;
  //   this.httpRequest.httpPut(this.url_user, json_user).subscribe(
  //     result => {
  //       try {
  //         const result_msg = JSON.parse(result._body);
  //         this.dataUser = result_msg;
  //         this.dataDinas = result_msg.dinas;
  //         this.dataBidang = result_msg.bidang;
  //         this.dataSeksi = result_msg.seksi;
  //         this.dataJabatan = result_msg.jabatan;
  //         this.dataPangkat = result_msg.pangkat;
  //         this.dataRole = result_msg.role;
  //         this.loading = false;
  //         this.showSuccess(result_msg['nama']);
  //       } catch (error) {
  //         this.loading = false;
  //         this.errorMessage.openErrorSwal('Something wrong.');
  //         console.log(error);
  //       }
  //     },
  //     error => {
  //       this.loading = false;
  //       console.log(error);
  //     }
  //   );
  // }

  // showSuccess(nama){
  //   swal({
  //     title: 'Informasi',
  //     text: 'User ' + nama + ' berhasil diubah.',
  //     type: 'success',
  //     allowOutsideClick: false
  //   }).then(() => {
  //     this.editProfile = false;
  //   });
  // }

  // buttonUbah() {
  //   this.editProfile = true;
  // }

  // buttonBatal() {
  //   this.editProfile = false;
  // }

  // // DATEPICKER ========================================================================================================
  // closeFix(event, datePicker) {
  //   if(event.target.offsetParent == null)
  //     datePicker.close();
  //   else if(event.target.offsetParent.nodeName != "NGB-DATEPICKER")
  //     datePicker.close();
  // }
}
