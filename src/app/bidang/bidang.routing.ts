import { Routes } from '@angular/router';

export const BidangRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Bidang',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './bidang-list/bidang-list.module#BidangListModule'
      },
      {
        path: 'add',
        loadChildren: './bidang-add/bidang-add.module#BidangAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './bidang-detail/bidang-detail.module#BidangDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './bidang-edit/bidang-edit.module#BidangEditModule'
      }
    ]
  }
]
