import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';
import { IOption } from "ng-select";

@Component({
  selector: 'app-bidang-add',
  templateUrl: './bidang-add.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class BidangAddComponent implements OnInit {

  public loading = false;
  private url_bidang = '/api/bidang';
  private url_dinas = '/api/dinas';

  myForm: FormGroup;
  submitted: boolean;

  dinasOption: Array<IOption> = [];

  simpleOption: Array<IOption> = [
    {value: '0', label: 'Alabama'},
    {value: '1', label: 'Wyoming'},
    {value: '2', label: 'Coming'},
    {value: '3', label: 'Henry Die'},
    {value: '4', label: 'John Doe'}
  ];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('bidang','add')){
      this.router.navigate(['/error/403']);
    }

    this.getDataDinasOption();

    this.myForm = new FormGroup({
      dinas: new FormControl({value: '', disabled: false}, [Validators.required]),
      nama_pendek: new FormControl({value: '', disabled: false}, [Validators.required]),
      nama_panjang: new FormControl({value: '', disabled: false}, [Validators.required]),
      email: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      is_deleted: new FormControl(),
      created_at: new FormControl(),
      updated_at: new FormControl(),
    });
  }

  getDataDinasOption() {
    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '?_sort=nama_pendek:ASC', json_dinas).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var dinasOpt = result_msg.map( function(dinas) {
            var option = { 
              value: dinas.id,
              label: dinas.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.dinasOption = dinasOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dinasOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dinasOption = null;
        console.log(error);
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    let val_bidang = this.myForm.value;

    this.myForm.patchValue({
      is_deleted: false,
      created_at: this.convert.formatDateYMDHMS(new Date().toString()),
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    });

    val_bidang = this.myForm.value;
    let json_bidang = JSON.stringify(val_bidang);

    this.loading = true;
    this.httpRequest.httpPost(this.url_bidang, json_bidang).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);

          this.loading = false;
          this.showSuccess(result_msg['nama_pendek']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'Bidang ' + nama + ' berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/bidang/list'])
    });
  }

}
