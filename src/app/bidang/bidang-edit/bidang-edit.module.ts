import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { BidangEditComponent } from './bidang-edit.component';
import { NgxLoadingModule } from 'ngx-loading';

export const BidangEditRoutes: Routes = [
  {
    path: '',
    component: BidangEditComponent,
    data: {
      breadcrumb: 'Ubah Bidang',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BidangEditRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [BidangEditComponent]
})
export class BidangEditModule { }
