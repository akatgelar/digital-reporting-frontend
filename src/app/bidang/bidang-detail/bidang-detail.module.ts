import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { BidangDetailComponent } from './bidang-detail.component';
import { NgxLoadingModule } from 'ngx-loading';

export const BidangDetailRoutes: Routes = [
  {
    path: '',
    component: BidangDetailComponent,
    data: {
      breadcrumb: 'Detail Bidang',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BidangDetailRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [BidangDetailComponent]
})
export class BidangDetailModule { }
