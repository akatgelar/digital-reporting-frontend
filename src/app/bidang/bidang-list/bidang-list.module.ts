import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { BidangListComponent } from './bidang-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const BidangListRoutes: Routes = [
  {
    path: '',
    component: BidangListComponent,
    data: {
      breadcrumb: 'List Bidang',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BidangListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [BidangListComponent]
})
export class BidangListModule { }
