import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { BidangRoutes } from './bidang.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(BidangRoutes),
      SharedModule
  ],
  declarations: []
})

export class BidangModule {}
