import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { LaporanRoutes } from './laporan.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(LaporanRoutes),
      SharedModule
  ],
  declarations: []
})

export class LaporanModule {}
