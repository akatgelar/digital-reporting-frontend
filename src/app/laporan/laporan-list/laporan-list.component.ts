import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';
import { IOption } from "ng-select";
import { NgbDateParserFormatter, NgbDateStruct, NgbCalendar } from "@ng-bootstrap/ng-bootstrap";

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
    one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
                    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
    !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
                    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

const now = new Date();

const TAHUN  = now.getFullYear();
const BULAN  = (now.getMonth() + 1);
const HARI   = now.getDate();

@Component({
  selector: 'app-laporan-list',
  templateUrl: './laporan-list.component.html',
  // styleUrls: ['../laporan.component.css'],
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class LaporanListComponent implements OnInit {

  laporanType: string;
  laporanOption: Array<IOption> = [
    {value: 'month', label: 'Bulan'},
    {value: 'range', label: 'Periode Tanggal'}
  ];
  tahunOption: Array<IOption> = [];
  bulanOption: Array<IOption> = [
    {value: '01', label: 'Januari'},
    {value: '02', label: 'Februari'},
    {value: '03', label: 'Maret'},
    {value: '04', label: 'April'},
    {value: '05', label: 'Mei'},
    {value: '06', label: 'Juni'},
    {value: '07', label: 'Juli'},
    {value: '08', label: 'Augustus'},
    {value: '09', label: 'September'},
    {value: '10', label: 'Oktober'},
    {value: '11', label: 'November'},
    {value: '12', label: 'Desember'},
  ];

  tahunNow: string = TAHUN.toString();
  bulanNow: string = (BULAN < 10) ? '0' + BULAN.toString() : BULAN.toString();

  hoveredDate: NgbDateStruct;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;

  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);
  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);

  constructor(
    private router: Router,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService,
    public parserFormatter: NgbDateParserFormatter
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('laporan','list')){
      this.router.navigate(['/error/403']);
    }

    for(var x = (TAHUN - 1); x <= (TAHUN + 1); ++x) {
      this.tahunOption.push({
        value: x.toString(), label: x.toString()
      });
    }
  }

  onLaporanChange(value) {
    this.laporanType = value;
  }

  onMonthChange(value) {
  }

  onDateChange(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

}
