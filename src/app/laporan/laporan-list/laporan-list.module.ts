import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { LaporanListComponent } from './laporan-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const LaporanListRoutes: Routes = [
  {
    path: '',
    component: LaporanListComponent,
    data: {
      breadcrumb: 'List Laporan',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LaporanListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [LaporanListComponent]
})
export class LaporanListModule { }
