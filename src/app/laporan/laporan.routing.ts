import { Routes } from '@angular/router';

export const LaporanRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Laporan',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './laporan-list/laporan-list.module#LaporanListModule'
      },
    ]
  }
]
