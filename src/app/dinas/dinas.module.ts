import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { DinasRoutes } from './dinas.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(DinasRoutes),
      SharedModule
  ],
  declarations: []
})

export class DinasModule {}
