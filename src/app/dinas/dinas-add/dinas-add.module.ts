import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { DinasAddComponent } from './dinas-add.component';
import { NgxLoadingModule } from 'ngx-loading';

export const DinasAddRoutes: Routes = [
  {
    path: '',
    component: DinasAddComponent,
    data: {
      breadcrumb: 'Tambah Dinas',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DinasAddRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [DinasAddComponent]
})
export class DinasAddModule { }
