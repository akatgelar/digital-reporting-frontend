import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-dinas-edit',
  templateUrl: './dinas-edit.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class DinasEditComponent implements OnInit {

  public loading = false;
  private url_dinas = '/api/dinas';

  dataDinas = [];

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('dinas','edit')){
      this.router.navigate(['/error/403']);
    }

    let id = this.route.snapshot.paramMap.get('id');
    this.getDataDinas(id);

    this.myForm = new FormGroup({
      nama_pendek: new FormControl({value: '', disabled: false}, [Validators.required]),
      nama_panjang: new FormControl({value: '', disabled: false}, [Validators.required]),
      alamat: new FormControl({value: '', disabled: false}, [Validators.required]),
      notelp: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[0-9]*$")]),
      email: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),
      website: new FormControl({value: '', disabled: false}),
      is_deleted: new FormControl(),
      updated_at: new FormControl(),
    });
  }

  getDataDinas(id) {
    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '/'+ id, json_dinas).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataDinas = result_msg;
          this.setForm(this.dataDinas);
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataDinas = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataDinas = null;
        console.log(error);
      }
    );
  }

  setForm(data){
    this.myForm.patchValue({
      id: data.id,
      nama_pendek: data.nama_pendek,
      nama_panjang: data.nama_panjang,
      alamat: data.alamat,
      notelp: data.notelp,
      email: data.email,
      website: data.website
    });
  }

  onSubmit() {
    let id = this.route.snapshot.paramMap.get('id');

    this.submitted = true;
    let val_dinas = this.myForm.value;

    this.myForm.patchValue({
      is_deleted: false,
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    });

    val_dinas = this.myForm.value;
    let json_dinas = JSON.stringify(val_dinas);

    this.loading = true;
    this.httpRequest.httpPut(this.url_dinas + '/' + id, json_dinas).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.showSuccess(result_msg['nama_pendek']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'Dinas ' + nama + ' berhasil diubah.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/dinas/list'])
    });
  }

}
