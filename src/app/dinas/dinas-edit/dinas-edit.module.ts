import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { DinasEditComponent } from './dinas-edit.component';
import { NgxLoadingModule } from 'ngx-loading';

export const DinasEditRoutes: Routes = [
  {
    path: '',
    component: DinasEditComponent,
    data: {
      breadcrumb: 'Ubah Dinas',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DinasEditRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [DinasEditComponent]
})
export class DinasEditModule { }
