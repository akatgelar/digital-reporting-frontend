import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { DinasListComponent } from './dinas-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const DinasListRoutes: Routes = [
  {
    path: '',
    component: DinasListComponent,
    data: {
      breadcrumb: 'List Dinas',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DinasListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [DinasListComponent]
})
export class DinasListModule { }
