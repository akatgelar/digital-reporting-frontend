import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { DinasDetailComponent } from './dinas-detail.component';
import { NgxLoadingModule } from 'ngx-loading';

export const DinasDetailRoutes: Routes = [
  {
    path: '',
    component: DinasDetailComponent,
    data: {
      breadcrumb: 'Detail Dinas',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DinasDetailRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [DinasDetailComponent]
})
export class DinasDetailModule { }
