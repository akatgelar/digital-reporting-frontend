import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { TemplateRoutes } from './template.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(TemplateRoutes),
      SharedModule
  ],
  declarations: []
})

export class TemplateModule {}
