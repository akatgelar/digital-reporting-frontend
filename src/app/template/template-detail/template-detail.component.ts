import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

@Component({
  selector: 'app-template-detail',
  templateUrl: './template-detail.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class TemplateDetailComponent implements OnInit {

  public loading = false;
  private url_template = '/api/template';

  dataDinas = [];
  dataBidang = [];
  dataTemplate = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('template','detail')){
      this.router.navigate(['/error/403']);
    }

    let id = this.route.snapshot.paramMap.get('id');
    this.getDataTemplate(id);
  }

  getDataTemplate(id) {
    const json_template = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_template + '/'+ id, json_template).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataTemplate = result_msg;
          this.dataBidang = result_msg.bidang;
          this.dataDinas = result_msg.dinas;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataTemplate = null;
          this.dataBidang = null;
          this.dataDinas = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataTemplate = null;
        this.dataBidang = null;
        this.dataDinas = null;
        console.log(error);
      }
    );
  }

}
