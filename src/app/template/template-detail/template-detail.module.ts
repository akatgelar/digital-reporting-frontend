import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { TemplateDetailComponent } from './template-detail.component';
import { NgxLoadingModule } from 'ngx-loading';

export const TemplateDetailRoutes: Routes = [
  {
    path: '',
    component: TemplateDetailComponent,
    data: {
      breadcrumb: 'Detail Template',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TemplateDetailRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [TemplateDetailComponent]
})
export class TemplateDetailModule { }
