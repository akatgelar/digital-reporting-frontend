import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { TemplateListComponent } from './template-list.component';
import { NgxLoadingModule } from 'ngx-loading';

export const TemplateListRoutes: Routes = [
  {
    path: '',
    component: TemplateListComponent,
    data: {
      breadcrumb: 'List Template',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TemplateListRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [TemplateListComponent]
})
export class TemplateListModule { }
