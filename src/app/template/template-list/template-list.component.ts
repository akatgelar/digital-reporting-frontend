import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import swal from 'sweetalert2';
import { IOption } from "ng-select";

@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class TemplateListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_template = '/api/template';

  rowsFilter = [];
  tempFilter = [];
  limit = 10;
  messages = {
    emptyMessage: 'Tidak ada data.',
    totalMessage: 'total'
  }

  pencarianType: string;
  pencarianInput: boolean = true;
  pencarianOption: Array<IOption> = [
    {value: 'dinas.nama_pendek', label: 'Dinas'},
    {value: 'bidang.nama_pendek', label: 'Bidang'},
    {value: 'nama', label: 'Nama'}
  ];

  public currentPageLimit: number = 10;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];

  constructor(
    private router: Router,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('template', 'list')){
      this.router.navigate(['/error/403']);
    }

    this.getData();
  }

  onSelectedPencarian(value) {
    this.pencarianType = value;
    this.pencarianInput = value == 0 ? true : false;
  }

  getData() {
    const json_template = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_template + '?is_deleted=false&_sort=updated_at:desc', json_template).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          this.rowsFilter = result_msg;
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }

  changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const filter = '&' + this.pencarianType + '_contains=' + val;

    const json_template = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_template + '?is_deleted=false&_sort=updated_at:desc' + filter, json_template).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          this.rowsFilter = result_msg;
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onDelete(row);
      }
    });
  }

  onDelete(row) {
    let json_template = {
      is_deleted: true,
      updated_at: new Date()
    };

    this.loading = true;
    this.httpRequest.httpPut(this.url_template + '/' + row['id'], json_template).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.showSuccess(row['nama']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      html: 'Template ' + nama + ' berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.getData();
    });
  }
}
