import { Routes } from '@angular/router';

export const TemplateRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Template',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './template-list/template-list.module#TemplateListModule'
      },
      {
        path: 'add',
        loadChildren: './template-add/template-add.module#TemplateAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './template-detail/template-detail.module#TemplateDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './template-edit/template-edit.module#TemplateEditModule'
      }
    ]
  }
]
