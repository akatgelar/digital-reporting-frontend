import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { TemplateEditComponent } from './template-edit.component';
import { NgxLoadingModule } from 'ngx-loading';

export const TemplateEditRoutes: Routes = [
  {
    path: '',
    component: TemplateEditComponent,
    data: {
      breadcrumb: 'Ubah Template',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TemplateEditRoutes),
    SharedModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [TemplateEditComponent]
})
export class TemplateEditModule { }
