import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';
import { IOption } from "ng-select";

@Component({
  selector: 'app-template-edit',
  templateUrl: './template-edit.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class TemplateEditComponent implements OnInit {

  public loading = false;
  private url_upload = '/api/upload'; 
  private url_template = '/api/template';
  private url_bidang = '/api/bidang';
  private url_dinas = '/api/dinas';

  dataTemplate = [];
  dinasOption: Array<IOption> = [];
  bidangOption: Array<IOption> = [];
  // bidangDinas: number;

  myForm: FormGroup;
  submitted: boolean;

  dinas: any;
  data_upload_doc: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('template','edit')){
      this.router.navigate(['/error/403']);
    }

    let id = this.route.snapshot.paramMap.get('id');
    this.getDataTemplate(id);

    this.getDataDinasOption();
    // this.getDataBidangOption(this.bidangDinas);

    this.myForm = new FormGroup({
      dinas: new FormControl({value: '', disabled: false}, [Validators.required]),
      bidang: new FormControl({value: '', disabled: false}, [Validators.required]),
      nama: new FormControl(),
      path: new FormControl(),
      is_deleted: new FormControl(),
      updated_at: new FormControl(),
    });
  }

  getDataTemplate(id) {
    const json_template = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_template + '/'+ id, json_template).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataTemplate = result_msg;
          this.getDataBidangOption(result_msg.dinas.id);
          this.data_upload_doc = {
            nama: result_msg.nama, 
            path: "/api" + result_msg.path
          }
          this.setForm(this.dataTemplate);
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dataTemplate = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dataTemplate = null;
        console.log(error);
      }
    );
  }

  setForm(data){
    this.myForm.patchValue({
      id: data.id,
      dinas: data.dinas.id.toString(),
      bidang: data.bidang.id.toString(),
      nama: data.nama,
      path: data.path
    });
  }

  getDataDinasOption() {
    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '?_sort=nama_pendek:ASC', json_dinas).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var dinasOpt = result_msg.map( function(dinas) {
            var option = {
              value: dinas.id.toString(),
              label: dinas.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.dinasOption = dinasOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dinasOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dinasOption = null;
        console.log(error);
      }
    );
  }

  getDataBidangOption(id_dinas) {
    const json_bidang = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_bidang + '?dinas=' + id_dinas + '&_sort=nama_pendek:ASC', json_bidang).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var bidangOpt = result_msg.map( function(bidang) {
            var option = {
              value: bidang.id.toString(),
              label: bidang.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.bidangOption = bidangOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.bidangOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.bidangOption = null;
        console.log(error);
      }
    );
  }

  onSelectedDinas(option: IOption) {
    const id_dinas = option.value;
    this.getDataBidangOption(id_dinas);
    this.myForm.patchValue({
      bidang: ''
    });
  }

  onDeselectedDinas(option: IOption) {
    this.bidangOption = null;
    this.myForm.controls['bidang'].reset();
  }

  onSubmit() {
    let id = this.route.snapshot.paramMap.get('id');

    this.submitted = true;
    let val_template = this.myForm.value;

    this.myForm.patchValue({
      is_deleted: false,
      updated_at: new Date()
    });

    val_template = this.myForm.value;
    let json_template = JSON.stringify(val_template);

    this.loading = true;
    this.httpRequest.httpPut(this.url_template + '/' + id, json_template).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.showSuccess(result_msg['nama']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'Template ' + nama + ' berhasil diubah.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/template/list'])
    });
  }

  onUpload(event, type, i) {
    let fileList: FileList = event.target.files; 
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData(); 
      formData.append('files', file, file.name);

      this.loading = true;
      this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);

            if(type.includes("doc") || type.includes("docx")){
              this.myForm.patchValue({
                nama: result_msg[0]['name'], 
                path: "/api" + result_msg[0]['url']
              });
              this.data_upload_doc = {
                nama: result_msg[0]['name'], 
                path: "/api" + result_msg[0]['url']
              }
            }

            this.loading = false; 
          } catch (error) {
            this.loading = false;
            console.log(error)
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
        );
    }
  }

}
