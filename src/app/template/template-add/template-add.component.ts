import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import swal from 'sweetalert2';
import { IOption } from "ng-select";

@Component({
  selector: 'app-template-add',
  templateUrl: './template-add.component.html',
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class TemplateAddComponent implements OnInit {
  public loading = false;
  private url_upload = '/api/upload'; 
  private url_template = '/api/template';
  private url_bidang = '/api/bidang';
  private url_dinas = '/api/dinas';

  myForm: FormGroup;
  submitted: boolean;

  upload_doc: FormArray; 
  data_upload_doc: any;

  dinasOption: Array<IOption> = [];
  bidangOption: Array<IOption> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('template','add')){
      this.router.navigate(['/error/403']);
    }

    this.getDataDinasOption();

    this.myForm = new FormGroup({
      dinas: new FormControl({value: '', disabled: false}, [Validators.required]),
      bidang: new FormControl({value: '', disabled: false}, [Validators.required]),
      nama: new FormControl(),
      path: new FormControl(),
      is_deleted: new FormControl(),
      created_at: new FormControl(),
      updated_at: new FormControl(),
    });
  }

  getDataDinasOption() {
    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '?_sort=nama_pendek:ASC', json_dinas).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var dinasOpt = result_msg.map( function(dinas) {
            var option = { 
              value: dinas.id,
              label: dinas.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.dinasOption = dinasOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.dinasOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.dinasOption = null;
        console.log(error);
      }
    );
  }

  onSelectedDinas(option: IOption) {
    const id_dinas = option.value;
    this.getDataBidangOption(id_dinas);
  }

  onDeselectedDinas(option: IOption) {
    this.bidangOption = null;
    this.myForm.controls['bidang'].reset();
  }

  getDataBidangOption(id_dinas) {
    const json_bidang = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_bidang + '?dinas=' + id_dinas + '&_sort=nama_pendek:ASC', json_bidang).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          var bidangOpt = result_msg.map( function(bidang) {
            var option = { 
              value: bidang.id,
              label: bidang.nama_pendek
            }
            return option;
          });
          this.loading = false;
          this.bidangOption = bidangOpt;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          this.loading = false;
          this.bidangOption = null;
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        this.bidangOption = null;
        console.log(error);
      }
    );
  }

  createUploadDOC(): FormGroup {
    return new FormGroup({
      name: new FormControl({value: '', disabled: false}, []), 
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }

  onUpload(event, type, i) {
    let fileList: FileList = event.target.files; 
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData(); 
      formData.append('files', file, file.name);

      this.loading = true;
      this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);

            if(type.includes("doc") || type.includes("docx")){
              this.myForm.patchValue({
                nama: result_msg[0]['name'], 
                path: "/api" + result_msg[0]['url']
              });
              this.data_upload_doc = {
                nama: result_msg[0]['name'], 
                path: "/api" + result_msg[0]['url']
              }
            }

            this.loading = false; 
          } catch (error) {
            this.loading = false;
            console.log(error)
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
        );
    }
  }

  onSubmit() {
    this.submitted = true;
    let val_template = this.myForm.value;

    this.myForm.patchValue({
      is_deleted: false,
      created_at: new Date(),
      updated_at: new Date()
    });

    val_template = this.myForm.value;
    let json_template = JSON.stringify(val_template);

    this.loading = true;
    this.httpRequest.httpPost(this.url_template, json_template).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);

          this.loading = false;
          this.showSuccess(result_msg['nama']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'Template ' + nama + ' berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/template/list'])
    });
  }
}
