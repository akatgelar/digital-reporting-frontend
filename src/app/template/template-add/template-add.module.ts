import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { TemplateAddComponent } from './template-add.component';
import { FileDropModule } from 'ngx-file-drop';
import { NgxLoadingModule } from 'ngx-loading';

export const TemplateAddRoutes: Routes = [
  {
    path: '',
    component: TemplateAddComponent,
    data: {
      breadcrumb: 'Tambah Template',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(TemplateAddRoutes),
    SharedModule,
    FileDropModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [TemplateAddComponent]
})
export class TemplateAddModule { }
