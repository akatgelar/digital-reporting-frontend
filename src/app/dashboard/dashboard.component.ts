import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { HttpRequestService } from '../shared/service/http-request.service';
import { SessionService } from '../shared/service/session.service';
import { ErrorMessageService } from '../shared/service/error-message.service';
import { ConverterService } from '../shared/service/converter.service';
import { fadeInOutTranslate } from '../shared/elements/animation';

import { ChartModule } from 'angular2-chartjs';
import 'chartjs-plugin-labels';

const now = new Date();
const TAHUN = now.getFullYear().toString();
const BULAN = (now.getMonth() + 1).toString();
const HARI = now.getDate().toString();

const TAHUN_NUM = now.getFullYear();
const BULAN_NUM = (now.getMonth() + 1);
const HARI_NUM = now.getDate();

const ARR_BULAN = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Augustus', 'September', 'Oktober', 'November', 'Desember'];
const ARR_BLN = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [
    HttpRequestService,
    SessionService,
    ErrorMessageService,
    ConverterService
  ]
})

export class DashboardComponent implements OnInit {

  public loading = false;
  public finish_loading = false;
  private url_user = '/api/users';
  private url_kegiatan = '/api/kegiatan';
  private url_evidence = '/api/evidence'; 

  tahun_pilih: string = TAHUN;
  bulan_pilih: string = (BULAN_NUM < 10) ? '0' + BULAN_NUM.toString() : BULAN_NUM.toString();
  bulan_jumlah: number = new Date(TAHUN_NUM, BULAN_NUM, 0).getDate();
  hari_jumlah: any[] = this.getDaysInMonth(TAHUN_NUM, BULAN_NUM);

  jumlah_user: number;
  jumlah_kegiatan: number;
  jumlah_evidence: number;

  jumlah_kegiatan_per_bulan: any[] = [];
  jumlah_evidence_per_bulan: any[] = [];
  jumlah_kegiatan_per_input: any[] = [];

  /*Bar chart*/
  typeBar = 'bar';
  dataBar = {
    labels: ARR_BLN,
    datasets: []
  };
  optionsBar = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
    	display: true,
      position: 'bottom',
    },
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
          stepSize: 5
        }
      }]
    },
    plugins: {
      labels: {
        render: function (args) {
          return '';
        }
      }
    }
  };

  /*Pie chart*/
  typePie = 'pie';
  dataPie = {
    // labels: ['Belum', 'Selesai'],
    labels: [],
    datasets: [{
      backgroundColor: [
        '#289D57',
        '#EF3C62'
      ],
      // data: [7, 15]
      data: []
    }]
  };
  optionsPie = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false,
      position: 'right',
    },
    plugins: {
      labels: {
        render: 'percentage',
        fontColor: '#fff'
      }
    }
  };

  /*Line chart*/
  typeLine = 'line';
  dataLine = {
    labels: [],
    datasets: [{
      label: 'Tanggal Kegiatan',
      backgroundColor: '#1786CD',
      borderColor: '#1786CD',
      fill: false,
      data: [],
    },{
      label: 'Tanggal Input',
      backgroundColor: '#EF3C62',
      borderColor: '#EF3C62',
      fill: false,
      data: [],
    }]
  };
  optionsLine = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: true,
      position: 'bottom',
    },
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
          stepSize: 5
        }
      }]
    },
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) {}

  ngOnInit() {
    if(!this.session.checkAccess('dashboard','list')){
      this.router.navigate(['/error/403']);
    }

    this.getJumlahKegiatan();
    this.getJumlahEvidence();
    this.getAllChart();
  }

  getJumlahKegiatan() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    // FILTER
    var t_awal = TAHUN + '-01-01';
    var awal = new Date(t_awal);
    var akhir = new Date(awal.setFullYear(awal.getFullYear() + 1));
    var t_akhir = akhir.getFullYear() + '-01-01';

    // const id = '3';
    const id             = sess['id'];
    const user           = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
    const is_deleted     = 'is_deleted=false';
    const tanggal_awal   = 'tanggal_gte=' + t_awal;
    const tanggal_akhir  = 'tanggal_lt=' + t_akhir;
    const filter         = '?' + user + is_deleted + '&' + tanggal_awal + '&' + tanggal_akhir;

    const json_kegiatan = {};
    var kegiatan = this.httpRequest.httpGet(this.url_kegiatan + filter, json_kegiatan)
    .toPromise()
    .then(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.jumlah_kegiatan = result_msg.length;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  getJumlahEvidence() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    // FILTER
    var t_awal = TAHUN + '-01-01';
    var awal = new Date(t_awal);
    var akhir = new Date(awal.setFullYear(awal.getFullYear() + 1));
    var t_akhir = akhir.getFullYear() + '-01-01';

    // const id = '3';
    const id             = sess['id'];
    const user           = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
    const is_deleted     = 'is_deleted=false';
    const tanggal_awal   = 'created_at_gte=' + t_awal;
    const tanggal_akhir  = 'created_at_lt=' + t_akhir;
    const filter         = '?' + user + is_deleted + '&' + tanggal_awal + '&' + tanggal_akhir;

    const json_evidence = {};
    var evidence = this.httpRequest.httpGet(this.url_evidence + filter, json_evidence)
    .toPromise()
    .then(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.jumlah_evidence = result_msg.length;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  async getAllChart() {
    this.loading = true;

    await this.getJumlahKegiatanByBulan();
    await this.getJumlahEvidenceByBulan();
    await this.getJumlahKegiatanByJumlahHari();
    // await this.getJumlahKegiatanByTanggalInput();
    await this.getJumlahKegiatanByTanggalInputNew();

    this.loading = false;
    this.finish_loading = true;
  }

  async getJumlahKegiatanByBulan() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    for(var x = 1; x <= ARR_BULAN.length; ++x) {
      var bln = (x < 10) ? '0' + x.toString() : x.toString();

      // FILTER
      var t_awal = TAHUN + '-' + bln + '-01';
      var awal = new Date(t_awal);
      var akhir = new Date(awal.setMonth(awal.getMonth() + 1));
      var y = akhir.getMonth() + 1;
      var bln_akhir = (y < 10) ? '0' + y.toString() : y.toString();
      var t_akhir = akhir.getFullYear() + '-' + bln_akhir + '-01';

      // const id = '3';
      const id             = sess['id'];
      const user           = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
      const is_deleted     = 'is_deleted=false';
      const tanggal_awal   = 'tanggal_gte=' + t_awal;
      const tanggal_akhir  = 'tanggal_lt=' + t_akhir;
      const filter         = '?' + user + is_deleted + '&' + tanggal_awal + '&' + tanggal_akhir;

      const json_kegiatan = {};
      var kegiatan = await this.httpRequest.httpGet(this.url_kegiatan + filter, json_kegiatan)
      .toPromise()
      .then(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            this.jumlah_kegiatan_per_bulan.push({
              bulan: x,
              hasil: result_msg.length
            });
          } catch (error) {
            this.errorMessage.openErrorSwal('Something wrong.');
            console.log(error);
          }
        },
        error => {
          console.log(error);
        }
      );
    }

    var finalArray = await this.jumlah_kegiatan_per_bulan.map(function (obj) {
      return obj.hasil;
    });

    var datasets = await this.dataBar.datasets.push({
      label: 'Kegiatan',
      backgroundColor: '#1786CD',
      data: finalArray,
    })
  }

  async getJumlahEvidenceByBulan() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    for(var x = 1; x <= ARR_BULAN.length; ++x) {
      var bln = (x < 10) ? '0' + x.toString() : x.toString();

      // FILTER
      var t_awal = TAHUN + '-' + bln + '-01';
      var awal = new Date(t_awal);
      var akhir = new Date(awal.setMonth(awal.getMonth() + 1));
      var y = akhir.getMonth() + 1;
      var bln_akhir = (y < 10) ? '0' + y.toString() : y.toString();
      var t_akhir = akhir.getFullYear() + '-' + bln_akhir + '-01';

      // const id = '3';
      const id             = sess['id'];
      const user           = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
      const is_deleted     = 'is_deleted=false';
      const tanggal_awal   = 'created_at_gte=' + t_awal;
      const tanggal_akhir  = 'created_at_lt=' + t_akhir;
      const filter         = '?' + user + is_deleted + '&' + tanggal_awal + '&' + tanggal_akhir;

      const json_evidence = {};
      var evidence = await this.httpRequest.httpGet(this.url_evidence + filter, json_evidence)
      .toPromise()
      .then(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            this.jumlah_evidence_per_bulan.push({
              bulan: x,
              hasil: result_msg.length
            });
          } catch (error) {
            this.errorMessage.openErrorSwal('Something wrong.');
            console.log(error);
          }
        },
        error => {
          console.log(error);
        }
      );
    }

    var finalArray = await this.jumlah_evidence_per_bulan.map(function (obj) {
      return obj.hasil;
    });

    var datasets = await this.dataBar.datasets.push({
      label: 'Evidence',
      backgroundColor: '#289D57',
      data: finalArray,
    })
  }

  async getJumlahKegiatanByJumlahHari() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    var t_awal = this.tahun_pilih + '-' + this.bulan_pilih + '-01';
    var awal = new Date(t_awal);
    var akhir = new Date(awal.setMonth(awal.getMonth() + 1));
    var y = akhir.getMonth() + 1;
    var bln_akhir = (y < 10) ? '0' + y.toString() : y.toString();
    var t_akhir = akhir.getFullYear() + '-' + bln_akhir + '-01';

    // const id = '3';
    const id             = sess['id'];
    const user           = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
    const is_deleted     = 'is_deleted=false';
    const tanggal_awal   = 'tanggal_gte=' + t_awal;
    const tanggal_akhir  = 'tanggal_lt=' + t_akhir;
    const filter         = '?' + user + is_deleted + '&' + tanggal_awal + '&' + tanggal_akhir;

    const json_kegiatan = {};
    var kegiatan = await this.httpRequest.httpGet(this.url_kegiatan + filter, json_kegiatan)
    .toPromise()
    .then(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          return result_msg;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        console.log(error);
      }
    );

    const reduced = kegiatan.reduce((obj, val) => {
      obj[val.tanggal] = obj[val.tanggal] || [];
      obj[val.tanggal].push(val);
      return obj;
    },{});

    let selesai = Object.keys(reduced).length;
    let belum = this.hari_jumlah.length - selesai;
    this.dataPie.labels.push('Selesai');
    this.dataPie.datasets[0].data.push(selesai);

    this.dataPie.labels.push('Belum');
    this.dataPie.datasets[0].data.push(belum);
  }

  async getJumlahKegiatanByTanggalInput() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    for(var x = 1; x <= this.bulan_jumlah; ++x) {
      var hr = (x < 10) ? '0' + x.toString() : x.toString();

      // FILTER
      var t_awal = TAHUN + '-' + this.bulan_pilih + '-' + hr;
      var awal = new Date(t_awal);
      var akhir = new Date(awal.setDate(awal.getDate() + 1));
      var y = akhir.getMonth() + 1;
      var bln_akhir = (y < 10) ? '0' + y.toString() : y.toString();
      var z = akhir.getDate();
      var hr_akhir = (z < 10) ? '0' + z.toString() : z.toString();
      var t_akhir = akhir.getFullYear() + '-' + bln_akhir + '-' + hr_akhir;

      // const id = '3';
      const id             = sess['id'];
      const user           = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
      const is_deleted     = 'is_deleted=false';
      const tanggal_awal   = 'tanggal_gte=' + t_awal;
      const tanggal_akhir  = 'tanggal_lt=' + t_akhir;
      const filter         = '?' + user + is_deleted + '&' + tanggal_awal + '&' + tanggal_akhir;

      const json_kegiatan = {};
      var kegiatan = await this.httpRequest.httpGet(this.url_kegiatan + filter, json_kegiatan)
      .toPromise()
      .then(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            this.dataLine.labels.push(hr);
            this.dataLine.datasets[0].data.push(result_msg.length);
          } catch (error) {
            this.errorMessage.openErrorSwal('Something wrong.');
            console.log(error);
          }
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  async getJumlahKegiatanByTanggalInputNew() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    var t_awal = this.tahun_pilih + '-' + this.bulan_pilih + '-01';
    var awal = new Date(t_awal);
    var akhir = new Date(awal.setMonth(awal.getMonth() + 1));
    var y = akhir.getMonth() + 1;
    var bln_akhir = (y < 10) ? '0' + y.toString() : y.toString();
    var t_akhir = akhir.getFullYear() + '-' + bln_akhir + '-01';

    // const id = '3';
    const id             = sess['id'];
    const user           = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
    const is_deleted     = 'is_deleted=false';
    const tanggal_awal   = 'created_at_gte=' + t_awal;
    const tanggal_akhir  = 'created_at_lt=' + t_akhir;
    const filter         = '?' + user + is_deleted + '&' + tanggal_awal + '&' + tanggal_akhir;

    const json_kegiatan = {};
    var kegiatan = await this.httpRequest.httpGet(this.url_kegiatan + filter, json_kegiatan)
    .toPromise()
    .then(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          return result_msg;
        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        console.log(error);
      }
    );

    for(var x = 1; x <= this.bulan_jumlah; ++x) {
      var hr = (x < 10) ? '0' + x.toString() : x.toString();
      var t_loop = new Date(this.tahun_pilih + '-' + this.bulan_pilih + '-' + hr);
      var tanggal_loop = t_loop.getFullYear() + '-' + t_loop.getMonth() + '-' + t_loop.getDate();

      var check_tgl_input = kegiatan.filter((res) => {
        var t_input = new Date(res.created_at);
        var tanggal_input = t_input.getFullYear() + '-' + t_input.getMonth() + '-' + t_input.getDate();
        return tanggal_input === tanggal_loop;
      }).length;

      var check_tgl_kegiatan = kegiatan.filter((res) => {
        var t_input = new Date(res.tanggal);
        var tanggal_input = t_input.getFullYear() + '-' + t_input.getMonth() + '-' + t_input.getDate();
        return tanggal_input === tanggal_loop;
      }).length;

      this.dataLine.labels.push(hr);
      this.dataLine.datasets[0].data.push(check_tgl_kegiatan);
      this.dataLine.datasets[1].data.push(check_tgl_input);
    }
  }

  getDaysInMonth(year, month) {
    month--;
    var date = new Date(year, month, 1);
    var days = [];
    while (date.getMonth() === month) {
      var tmpDate = new Date(date);            
      var weekDay = tmpDate.getDay();
      var day = tmpDate.getDate();

      if (weekDay%6) {
        days.push(day);
      }

      date.setDate(date.getDate() + 1);
    }

    return days;
  }  
}
