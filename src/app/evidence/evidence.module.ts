import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { EvidenceRoutes } from './evidence.routing';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(EvidenceRoutes),
      SharedModule
  ],
  declarations: []
})

export class EvidenceModule {}
