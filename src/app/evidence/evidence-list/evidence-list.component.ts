import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ConverterService } from '../../shared/service/converter.service';

import { DatatableComponent } from '@swimlane/ngx-datatable';
import swal from 'sweetalert2';
import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { IOption } from "ng-select";

@Component({
  selector: 'app-evidence-list',
  templateUrl: './evidence-list.component.html',
  styleUrls: ['../evidence.component.css'],
  providers: [
    HttpRequestService,
    ErrorMessageService,
    ConverterService
  ]
})
export class EvidenceListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_upload = '/api/upload';
  private url_evidence = '/api/evidence';

  is_list = false;
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 

  rowsFilter = [];
  fileUploads: any[] = []; 
  // tempFilter = [];
  // limit = 10;
  // messages = {
  //   emptyMessage: 'Tidak ada data.',
  //   totalMessage: 'total'
  // }

  fil: string = '';
  pencarianType: string;
  pencarianInput: boolean = true;
  pencarianOption: Array<IOption> = [
    {value: 'nama', label: 'Nama'},
    {value: 'is_public', label: 'Tipe'}
  ];

  public currentPageLimit: number = 0;
  // public pageLimitOptions = [
  //   {value: 5},
  //   {value: 10},
  //   {value: 25},
  //   {value: 50},
  //   {value: 100},
  // ];

  constructor(
    private router: Router,
    private _location: Location,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private convert: ConverterService
  ) { }

  ngOnInit() {
    if(!this.session.checkAccess('kegiatan','list')){
      this.router.navigate(['/error/403']);
    }
 
    this.is_list = this.session.checkAccess('kegiatan','list');
    this.is_detail = this.session.checkAccess('kegiatan','detail');
    this.is_add = this.session.checkAccess('kegiatan','add');
    this.is_edit = this.session.checkAccess('kegiatan','edit');
    this.is_delete = this.session.checkAccess('kegiatan','delete'); 

    this.getData();
  }

  // onLimitChange(limit: any): void {
  //   this.changePageLimit(limit);
  //   this.table.limit = this.currentPageLimit;
  //   this.table.recalculate();
  //   setTimeout(() => {
  //     if (this.table.bodyComponent.temp.length <= 0) {
  //       this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
  //     }
  //   });
  // }

  // changePageLimit(limit: any): void {
  //   this.currentPageLimit = parseInt(limit, 10);
  // }

  updateFilter(event: any) {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    if(this.pencarianType == 'is_public'){
      if(!event){
         var pan = '';
      } else {
        var val = event;
        var pan: string = val;
        var fil = '&is_public=' + val;
        this.fil = fil;
      }
    } else if(this.pencarianType == 'nama'){
      var val = event.target.value.toLowerCase();
      var pan: string = val;
      var fil = '&nama_contains=' + val;
      this.fil = fil;
    }

    // const id = '3';
    const id         = sess['id'];
    const user       = (sess['role']['type'] == 'pegawai') ? '&user=' + id : '';
    
    this.currentPageLimit = 0;

    const current = '&_start=' + this.currentPageLimit;
    const limit = '&_limit=4';

    if(pan.length > 0){
      var filter = fil;
    } else {
      var filter = '';
    }

    const json_jabatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_evidence + '?is_deleted=false&_sort=updated_at:desc' + user + filter + current + limit, json_jabatan).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          const jumlah = result_msg.length;

          this.rowsFilter = [];
          const newArray = result_msg.map(o => {
            this.rowsFilter.push({
              id: o.id,
              nama: o.nama,
              path: "/api" + o.path,
              is_public: o.is_public,
            });
          });

          if(jumlah > 0){
            this.currentPageLimit = this.currentPageLimit + jumlah;
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  getData() {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    // const id = '3';
    const id         = sess['id'];
    const user       = (sess['role']['type'] == 'pegawai') ? 'user=' + id + '&' : '';
    const is_deleted = 'is_deleted=false';
    const start      = '_start=' + this.currentPageLimit;
    const limit      = '_limit=4';
    const sort       = '_sort=updated_at:desc';
    const filter     = '?' + user + is_deleted + '&' + start + '&' + limit + '&' + sort;
    const filter2    = this.fil;

    const json_evidence = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_evidence + filter + filter2, json_evidence).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          const jumlah = result_msg.length;

          const newArray = result_msg.map(o => {
            this.rowsFilter.push({
              id: o.id,
              nama: o.nama,
              path: "/api" + o.path,
              is_public: o.is_public,
            });
          });

          if(jumlah > 0){
            this.currentPageLimit = this.currentPageLimit + jumlah;
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  loadmoreFilter(){
    this.getData();
  }

  // UPLOAD ============================================================================================================
  dropped(event: UploadEvent) {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    for (const droppedFile of event.files) {
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {

          // Here you can access the real file
          // console.log(droppedFile.relativePath, file);

          // You could upload it like this:
          const formData = new FormData()
          formData.append('files', file, droppedFile.relativePath)

          // Upload
          this.loading = true;
          this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
            result => {
              try {
                this.loading = false;
                const result_msg = JSON.parse(result._body);

                // Evidence
                const json_evidence = {
                  nama: result_msg[0].name,
                  path: result_msg[0].url,
                  user: sess['id'],
                  dari: 'website',
                  is_public: false,
                  is_deleted: false,
                  tahun: this.convert.formatDateY(new Date().toString()),
                  bulan: this.convert.formatDateM(new Date().toString()),
                  hari: this.convert.formatDateD(new Date().toString()),
                  created_at: this.convert.formatDateYMDHMS(new Date().toString()),
                  updated_at: this.convert.formatDateYMDHMS(new Date().toString())
                };
                this.loading = true;
                this.httpRequest.httpPost(this.url_evidence, json_evidence).subscribe(
                  result => {
                    try {
                      this.loading = false;
                      const result_msg = JSON.parse(result._body);
                      // this.fileUploads.push([
                      //   value => result_msg.id,
                      //   name => result_msg.nama,
                      // ]);
                      this.fileUploads.push({
                        value: result_msg.id,
                        name: result_msg.nama,
                        path: "/api" + result_msg.path
                      });
                    } catch (error) {
                      this.loading = false;
                      this.errorMessage.openErrorSwal('Something wrong.');
                      console.log(error);
                    }
                  },
                  error => {
                    this.loading = false;
                    console.log(error);
                  }
                );

              } catch (error) {
                this.loading = false;
                console.log(error);
              }
            },
            error => {
              this.loading = false;
              console.log(error);
            }
          );

        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  droppedMultiple(event) {
    // SESSION
    const sess = JSON.parse(this.session.getData());

    for (var i = 0; i < event.target.files.length; i++) { 
      const formData = new FormData();
      formData.append("files", event.target.files[i]);

      // Upload
      this.loading = true;
      this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
        result => {
          try {
            this.loading = false;
            const result_msg = JSON.parse(result._body);

            // Evidence
            const json_evidence = {
              nama: result_msg[0].name,
              path: result_msg[0].url,
              user: sess['id'],
              dari: 'website',
              is_public: false,
              is_deleted: false,
              tahun: this.convert.formatDateY(new Date().toString()),
              bulan: this.convert.formatDateM(new Date().toString()),
              hari: this.convert.formatDateD(new Date().toString()),
              created_at: this.convert.formatDateYMDHMS(new Date().toString()),
              updated_at: this.convert.formatDateYMDHMS(new Date().toString())
            };
            this.loading = true;
            this.httpRequest.httpPost(this.url_evidence, json_evidence).subscribe(
              result => {
                try {
                  this.loading = false;
                  const result_msg = JSON.parse(result._body);
                  // this.fileUploads.push([
                  //   value => result_msg.id,
                  //   name => result_msg.nama,
                  // ]);
                  this.fileUploads.push({
                    value: result_msg.id,
                    name: result_msg.nama,
                    path: "/api" + result_msg.path
                  });
                } catch (error) {
                  this.loading = false;
                  this.errorMessage.openErrorSwal('Something wrong.');
                  console.log(error);
                }
              },
              error => {
                this.loading = false;
                console.log(error);
              }
            );

          } catch (error) {
            this.loading = false;
            console.log(error);
          }
        },
        error => {
          this.loading = false;
          console.log(error);
        }
      );
    }
  }

  public fileOver(event){
    console.log(event);
  }
 
  public fileLeave(event){
    console.log(event);
  }

  // CLEAR GAMBAR ======================================================================================================
  clearUploadGambar(){
    this.fileUploads = [];

    this.rowsFilter = [];
    this.currentPageLimit = 0;
    this.getData();
  }

  // DELETE ============================================================================================================
  showDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onDelete(row);
      }
    });
  }

  onDelete(row) {
    let json_evidence = {
      is_deleted: true,
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    };

    this.loading = true;
    this.httpRequest.httpPut(this.url_evidence + '/' + row.id, json_evidence).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          this.showSuccess(row['nama']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      html: 'Evidence ' + nama + ' berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.rowsFilter = [];
      this.currentPageLimit = 0;
      this.getData();
    });
  }

  // PUBLIC ============================================================================================================
  showPublic(event, row, tipe) {
    let jenis = tipe == true ? 'Non-public' : 'Public';
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan mengubah data ini menjadi " + jenis + "?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onPublic(row, tipe);
      }
    });
  }

  onPublic(row, tipe) {
    let jenis = tipe == true ? false : true;
    let json_evidence = {
      is_public: jenis,
      updated_at: this.convert.formatDateYMDHMS(new Date().toString())
    };

    this.loading = true;
    this.httpRequest.httpPut(this.url_evidence + '/' + row.id, json_evidence).subscribe(
      result => {
        try {
          this.loading = false;
          const result_msg = JSON.parse(result._body);
          this.showSuccessPublic(row['nama']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        this.loading = false;
        console.log(error);
      }
    );
  }

  showSuccessPublic(nama){
    swal({
      title: 'Informasi',
      html: 'Evidence ' + nama + ' berhasil diubah.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.rowsFilter = [];
      this.currentPageLimit = 0;
      this.getData();
    });
  }

  onSelectedPencarian(value) {
    this.pencarianType = value;
    this.pencarianInput = value == 0 ? true : false;
  }

}
