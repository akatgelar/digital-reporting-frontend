import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { SharedModule } from '../../shared/shared.module';
import { EvidenceListComponent } from './evidence-list.component';
import { FileDropModule } from 'ngx-file-drop';
import { NgxLoadingModule } from 'ngx-loading';

export const EvidenceListRoutes: Routes = [
  {
    path: '',
    component: EvidenceListComponent,
    data: {
      breadcrumb: 'List Evidence',
      status: true
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(EvidenceListRoutes),
    SharedModule,
    FileDropModule,
    NgxLoadingModule.forRoot({}),
  ],
  declarations: [EvidenceListComponent]
})
export class EvidenceListModule { }
