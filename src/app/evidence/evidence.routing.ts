import { Routes } from '@angular/router';

export const EvidenceRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Evidence',
      status: false
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './evidence-list/evidence-list.module#EvidenceListModule'
      },
    ]
  }
]
